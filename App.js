import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {useScreens} from 'react-native-screens';

import Login from './src/screens/login';
import PilihRole from './src/screens/pilihRole';
import Home from './src/screens/home';
import AddSuratMasuk from './src/screens/addSuratMasuk';
import ListSurat from './src/screens/listSuratMasuk';
import DetailSuratMasuk from './src/screens/detailSuratMasuk';
import AddDisposisi from './src/screens/addDisposisi';
import ListDisposisi from './src/screens/listDisposisi';
import DetailDisposisiMasuk from './src/screens/detailDisposisiMasuk';
import AddInformasi from './src/screens/addInformasi';
import ListInformasi from './src/screens/listInformasi';
import DetailInformasiMasuk from './src/screens/detailInformasiMasuk';

// import CobaDownload from './src/screens/CobaDownload';
// import CobaUploadFile from './src/screens/CobaUploadFile';

useScreens();

const stackConfig = {
  headerStyle: {
    backgroundColor: '#fff',
    elevation: 0,
  },
  headerTintColor: '#424242',
  headerTitleStyle: {
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 22,
    marginLeft: 0,
  },
};

const stackNavigator = createStackNavigator(
  {
    Home,
    AddSuratMasuk,
    ListSurat,
    DetailSuratMasuk,
    AddDisposisi,
    ListDisposisi,
    DetailDisposisiMasuk,
    AddInformasi,
    ListInformasi,
    DetailInformasiMasuk,
  },

  {defaultNavigationOptions: stackConfig},
);

const switchNavigator = createSwitchNavigator({
  // CobaDownload,
  // CobaUploadFile,
  Login,
  PilihRole,

  stackNavigator,
});

export default createAppContainer(switchNavigator);
