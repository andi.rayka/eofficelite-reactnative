import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {addCheckedValueToObj} from 'rootapp/src/commonFunctions';

const apiAuth = axios.create({
  baseURL: 'http://139.59.241.147/eofficelite/index.php/api/authentication',
  data: {device: 'android'},
});

const apiContent = axios.create({
  baseURL: 'http://139.59.241.147/eofficelite/index.php/api/content',
  data: {device: 'android'},
});

const apiMaster = axios.create({
  baseURL: 'http://139.59.241.147/eofficelite/index.php/api/master',
  data: {device: 'android'},
});

const apiActivity = axios.create({
  baseURL: 'http://139.59.241.147/eofficelite/index.php/api/ContentActivity',
  data: {device: 'android'},
});

const apiAttachment = axios.create({
  baseURL: 'http://139.59.241.147/eofficelite/index.php/api/Attachment',
  data: {device: 'android'},
});

export {apiAuth, apiContent, apiMaster, apiActivity, apiAttachment};

// Call api Master yang memiliki 2 parameter
export const apiMaster2Param = async (
  url,
  successCode,
  setFetchedDataFuncName,
) => {
  const token = await AsyncStorage.getItem('mainLoginToken');

  await axios
    .post(`http://139.59.241.147/eofficelite/index.php/api/master${url}`, {
      token,
      device: 'android',
    })
    .then(resp => {
      const {code, error_message} = resp.data.system;
      const {data} = resp.data;

      if (code === successCode) {
        let newVal = addCheckedValueToObj(data);

        setFetchedDataFuncName(newVal);
        console.log('Master value: ', newVal);
      } else {
        alert(error_message);
      }
    })
    .catch(error => {
      alert('Gagal mengambil data');
      console.log(error);
    });
};
