import AsyncStorage from '@react-native-community/async-storage';
import RNFetchBlob from 'rn-fetch-blob';
import {PermissionsAndroid} from 'react-native';
import {apiAttachment} from 'rootapp/src/services/api';

// Set Data ke AsyncStorage
// -> Key AsyncStorage (harus unique) dan value yang disimpan
export const setAsyncStorage = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, value);
  } catch (error) {
    console.log(error);
  }
};

// Tambah Value "isChecked" dgn default false ke tiap object dalam array
// -> Array yang berisi object-object tsb
export const addCheckedValueToObj = data => {
  let arr = data;
  for (let i = 0; i < data.length; i++) {
    arr[i] = {...arr[i], isChecked: false};
  }

  return arr;
};

// Download Lampiran
// -> id lampiran/attachment
export const downloadLampiran = async idAttachment => {
  try {
    // izin ke pengguna
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        title: 'Izin menyimpan file',
        message: 'Aplikasi meminta izin mengakses penyimpanan',
        buttonNeutral: 'Tanya lagi nanti',
        buttonNegative: 'Tolak',
        buttonPositive: 'Izinkan',
      },
    );
    // Jika diizinkan
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      const token = await AsyncStorage.getItem('mainLoginToken');

      // Get Base64 dari server
      await apiAttachment
        .post('/getAttachment', {
          token,
          idAttachment,
        })
        .then(resp => {
          const {code, error_message} = resp.data.system;
          const {filename, filecontent} = resp.data.data;

          // Jika get base64 berhasil
          // Convert base64 ke file
          if (code === 234) {
            let dirs = RNFetchBlob.fs.dirs;
            let path = `${dirs.DownloadDir}/${filename}`;
            RNFetchBlob.fs
              .writeFile(path, filecontent, 'base64')
              .then(res => {
                alert('File berhasil diunduh di folder download');
              })
              .catch(error => {
                alert('Gagal menyimpan data ke penyimpanan');
                console.log(error);
              });
          } else {
            alert(error_message);
          }
        })
        .catch(error => {
          alert('Gagal download data dari server');
          console.log(error);
        });
      // Jika tidak diizinkan
    } else {
      alert('Tidak mendapat izin sehingga tidak bisa menyimpan file');
    }
  } catch (err) {
    console.log(err);
  }
};
