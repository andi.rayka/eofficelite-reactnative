import React from 'react';
import {View, Text, TouchableOpacity, FlatList, StyleSheet} from 'react-native';

import Icon from 'react-native-vector-icons/Feather';

import {withNavigation} from 'react-navigation';

const ListInformasiItem = ({data, navigation: {navigate}}) => {
  // Style untuk view row
  const rowStyles = {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  };

  const renderItem = ({item}) => {
    const {idcontentactivity, commands_text, contentactivitydate} = item;

    const newYYMMDD = contentactivitydate.substr(0, 10);
    const newcommands_text = commands_text.replace(/_##_/g, '; ');
    return (
      <TouchableOpacity
        activeOpacity={0.6}
        style={styles.containerList}
        onPress={() => navigate('DetailInformasiMasuk', {idcontentactivity})}>
        <View>
          {/* Pengirim & tgl */}
          <View style={rowStyles}>
            <Text
              numberOfLines={1}
              ellipsizeMode="tail"
              style={{
                fontSize: 18,
                fontFamily: 'SourceSansPro-SemiBold',
                color: '#212121',
              }}>
              {item.textint_act}
            </Text>
            <Text
              style={{
                fontSize: 16,
                fontFamily: 'SourceSansPro-SemiBold',
                color: '#424242',
              }}>
              {newYYMMDD}
            </Text>
          </View>
          {/* Commands  */}
          <Text
            numberOfLines={1}
            ellipsizeMode="tail"
            style={{
              fontSize: 16,
              fontFamily: 'SourceSansPro-SemiBold',
              color: '#212121',
            }}>
            {newcommands_text}
          </Text>
          {/* Note */}
          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                fontSize: 16,
                fontFamily: 'SourceSansPro-SemiBold',
                color: '#00ce4e',
              }}>
              Note:
            </Text>
            <Text
              numberOfLines={1}
              ellipsizeMode="tail"
              style={{
                fontSize: 16,
                fontFamily: 'SourceSansPro-Regular',
                color: '#212121',
              }}>
              {' ' + item.notes}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  const itemSeparator = () => {
    return (
      <>
        <View style={{backgroundColor: '#2ccce4', height: 2, width: '100%'}} />
        <View style={{backgroundColor: '#f5f5f5', height: 7, width: '100%'}} />
      </>
    );
  };

  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      data={data}
      renderItem={renderItem}
      ItemSeparatorComponent={itemSeparator}
      keyExtractor={(item, key) => key.toString()}
    />
  );
};

const styles = StyleSheet.create({
  containerList: {
    flexDirection: 'row',
    paddingTop: 19,
    paddingBottom: 20,
    paddingHorizontal: 15,
    backgroundColor: '#FFFFFF',
  },
  containerTextAtas: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  containerBarisBawah: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8,
    justifyContent: 'space-between',
  },
  containerLampiran: {
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 0.5,
    borderColor: '#38ee7d',
    padding: 3,
  },
  containerStatus: {
    // backgroundColor: '#37d67a',
    backgroundColor: '#ff8a65',
    borderRadius: 20,
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  iconDisposisi: {
    width: 40,
    height: 30,
  },
  textPengirim: {
    flex: 1,
    fontSize: 16,
    color: '#424242',
    fontFamily: 'SourceSansPro-SemiBold',
  },
  textTraitCode: {
    fontSize: 13,
    color: '#fff',
    fontFamily: 'SourceSansPro-SemiBold',
    textAlign: 'center',
    backgroundColor: '#2ccce4',
    width: '100%',
    marginTop: 2,
    padding: 1,
    borderRadius: 1.5,
  },
  textTgl: {
    fontSize: 14,
    color: '#424242',
    fontFamily: 'SourceSansPro-SemiBold',
    marginLeft: 10,
  },
  textIsi: {
    fontSize: 14,
    color: '#424242',
    fontFamily: 'SourceSansPro-SemiBold',
  },
  textLampiran: {
    fontSize: 16,
    color: '#424242',
    fontFamily: 'SourceSansPro-Regular',
    marginLeft: 5,
  },
  textStatus: {
    fontSize: 13,
    color: '#FFFFFF',
    fontFamily: 'SourceSansPro-SemiBold',
  },
});

export default withNavigation(ListInformasiItem);
