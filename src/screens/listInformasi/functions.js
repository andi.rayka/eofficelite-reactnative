import AsyncStorage from '@react-native-community/async-storage';
import {apiActivity} from 'rootapp/src/services/api';

// Fetch pertama kali dengan filter default
export const InitialFetchList = async (func1, func2) => {
  const token = await AsyncStorage.getItem('mainLoginToken');

  await apiActivity
    .post('/getInformationInList', {
      token,
      keyword: '',
      dateStart: '01-01-2019',
      dateEnd: '31-12-2019',

      isContentNumber: 0,
      isSubject: 0,
      isContentActivityCode: 0,
      isContentSender: 0,
      isSender: 0,
      isRecipient: 0,
      isRead: 0,
      isUnread: 0,
      isReported: 0,
      isForwarded: 0,
      isForSuperior: 0,
      isNoAttachment: 0,
    })
    .then(resp => {
      const {code, error_message} = resp.data.system;
      const {rows} = resp.data.data;

      if (code === 243) {
        func1(rows);
        console.log('list informasi: ', rows);
      } else {
        alert(error_message);
      }

      func2(false);
    })
    .catch(error => {
      alert('Gagal mengambil data');
      console.log(error);
    });
};
