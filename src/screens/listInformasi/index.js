import React, {useState, useEffect, useRef} from 'react';
import {
  StatusBar,
  Text,
  TouchableOpacity,
  CheckBox,
  ScrollView,
  View,
  StyleSheet,
  Dimensions,
  Modal,
  TextInput,
} from 'react-native';

import {InitialFetchList} from './functions';
import AsyncStorage from '@react-native-community/async-storage';
import {apiActivity} from 'rootapp/src/services/api';

import {
  NavHeaderButtons,
  Item,
} from 'rootapp/src/components/navigation/HeaderButton';
import LinearGradient from 'react-native-linear-gradient/';
import LoadingScreen from 'rootapp/src/components/common/LoadingScreen';
import ItemInformasi from './Item';
import DatePicker1 from 'rootapp/src/components/form/DatePicker';
import DatePicker2 from 'rootapp/src/components/form/DatePicker';
import IcFontisto from 'react-native-vector-icons/Fontisto';

const ListInformasi = ({navigation}) => {
  const [fetchedData, setFetchedData] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const [keyword, setKeyword] = useState('');

  // Date
  const [date1, setDate1] = useState(new Date());
  const [stringDate1, setStringDate1] = useState('01-01-2019');
  const [isShowDate1, toggleShowDate1] = useState(false);
  const [date2, setDate2] = useState(new Date());
  const [stringDate2, setStringDate2] = useState('31-12-2019');
  const [isShowDate2, toggleShowDate2] = useState(false);

  // Independet Filter Value
  const [isRead, toggleIsRead] = useState(false);
  const [isUnread, toggleIsUnread] = useState(false);
  const [isReported, toggleIsReported] = useState(false);
  const [isForwarded, toggleIsForwarded] = useState(false);

  // Dependent Search Value
  const [isContentNumber, toggleIsContentNumber] = useState(false);
  const [isSubject, toggleIsSubject] = useState(false);
  const [isContentActivityCode, toggleisContentActivityCode] = useState(false);
  const [isContentSender, toggleisContentSender] = useState(false);
  const [isSender, toggleisSender] = useState(false);
  const [isRecipient, toggleisRecipient] = useState(false);

  // Is Modal Open
  const [isModalSearchOpen, toggleModalSearchOpen] = useState(false);
  const [isModalFilterOpen, toggleModalFilter] = useState(false);

  // Initial fetch dengan filter default
  useEffect(() => {
    InitialFetchList(setFetchedData, setLoading);
  }, []);

  const {params = {}} = navigation.state;
  const paramDesc = useRef(params.paramDesc);
  const setParam = useRef(navigation.setParams);

  useEffect(() => {
    setParam.current({openSearch: () => toggleModalSearchOpen(true)});
    setParam.current({openFilter: () => toggleModalFilter(true)});
  }, [paramDesc, setParam]);

  // Handle klik filter, berjalan tiap kali filter diklik
  const handleIndependentFilter = type => {
    switch (type) {
      case 'semua':
        toggleIsRead(false);
        toggleIsUnread(false);
        toggleIsReported(false);
        toggleIsForwarded(false);
        break;

      case 'isRead':
        toggleIsRead(true);
        toggleIsUnread(false);
        toggleIsReported(false);
        toggleIsForwarded(false);
        break;

      case 'isUnread':
        toggleIsRead(false);
        toggleIsUnread(true);
        toggleIsReported(false);
        toggleIsForwarded(false);
        break;

      case 'isReported':
        toggleIsRead(false);
        toggleIsUnread(false);
        toggleIsReported(true);
        toggleIsForwarded(false);
        break;

      case 'isForwarded':
        toggleIsRead(false);
        toggleIsUnread(false);
        toggleIsReported(false);
        toggleIsForwarded(true);
        break;

      default:
        alert('salah input');
        break;
    }
    console.log('~~~~~~~~~');
    console.log(+isRead);
    console.log(+isUnread);
    console.log(+isReported);
    console.log(+isForwarded);

    applySearch();
  };

  // Terapkan search dari modal search
  const applySearch = async () => {
    const token = await AsyncStorage.getItem('mainLoginToken');

    setLoading(true);

    console.log('~~~~~~~~~');
    console.log(+isRead);
    console.log(+isUnread);
    console.log(+isReported);
    console.log(+isForwarded);
    await apiActivity
      .post('/getDispositionInList', {
        token,
        keyword, // error gak bisa terima value, sementara
        dateStart: stringDate1,
        dateEnd: stringDate2,

        isContentNumber: +isContentNumber,
        isSubject: +isSubject,
        isContentActivityCode: +isContentActivityCode,
        isContentSender: +isContentSender,
        isSender: +isSender,
        isRecipient: +isRecipient,
        isRead: +isRead,
        isUnread: 0,
        isReported: +isReported,
        isForwarded: +isForwarded,
        isForSuperior: 0,
      })
      .then(resp => {
        const {code, error_message} = resp.data.system;
        const {rows} = resp.data.data;

        if (code === 238) {
          setFetchedData(rows);
          setLoading(false);
          toggleModalSearchOpen(false);
          toggleModalFilter(false);
        } else {
          alert(error_message);
        }
      })
      .catch(error => {
        alert('Gagal mengambil data');
        console.log(error);
      });
  };

  // handle date start
  const handledate1 = (event, pickedDate) => {
    toggleShowDate1(false);

    if (pickedDate) {
      let dd = String(pickedDate.getDate()).padStart(2, '0');
      let mm = String(pickedDate.getMonth() + 1).padStart(2, '0'); //January = 0
      let yyyy = pickedDate.getFullYear();

      let fullDate = dd + '-' + mm + '-' + yyyy;

      setDate1(pickedDate);
      setStringDate1(fullDate);
    }
  };

  // handle date end
  const handledate2 = (event, pickedDate) => {
    toggleShowDate2(false);

    if (pickedDate) {
      let dd = String(pickedDate.getDate()).padStart(2, '0');
      let mm = String(pickedDate.getMonth() + 1).padStart(2, '0'); //January = 0
      let yyyy = pickedDate.getFullYear();

      let fullDate = dd + '-' + mm + '-' + yyyy;

      setDate2(pickedDate);
      setStringDate2(fullDate);
    }
  };

  // Reset Search Seperti semula
  const resetSearch = () => {
    setKeyword('');
    toggleIsContentNumber(false);
    toggleIsSubject(false);
    toggleisContentActivityCode(false);
    toggleisContentSender(false);
    toggleisSender(false);
    toggleisRecipient(false);
  };

  // Memberi nilai true/false pada CheckBox filter search
  const handleCheckFilterSearch = type => {
    switch (type) {
      case 'isContentNumber':
        toggleIsContentNumber(!isContentNumber);
        break;

      case 'isSubject':
        toggleIsSubject(!isSubject);
        break;

      case 'isContentActivityCode':
        toggleisContentActivityCode(!isContentActivityCode);
        break;

      case 'isContentSender':
        toggleisContentSender(!isContentSender);
        break;

      case 'isSender':
        toggleisSender(!isSender);
        break;

      case 'isRecipient':
        toggleisRecipient(!isRecipient);
        break;

      default:
        alert('salah input');
        break;
    }
  };

  return isLoading ? (
    <LoadingScreen />
  ) : (
    <>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />

      {/* Item List Informarsi */}
      <ItemInformasi data={fetchedData} />

      {/* Tombol Sementara  */}
      {/* <View
        style={{
          position: 'absolute',
          bottom: 0,
          width: '100%',
        }}> */}
      {/* Filter disposisi  */}
      {/* <TouchableOpacity onPress={() => toggleModalFilter(true)}>
          <LinearGradient
            style={{
              paddingTop: 7,
              paddingBottom: 10,
              borderRadius: 2,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={['#38ee7d', '#11988d']}>
            <Text
              style={{
                fontSize: 18,
                color: '#FFFFFF',
                fontFamily: 'SourceSansPro-Semibold',
              }}>
              Filter Disposisi
            </Text>
          </LinearGradient>
        </TouchableOpacity> */}

      {/* Search Disposisi */}
      {/* <TouchableOpacity onPress={() => toggleModalSearchOpen(true)}>
          <LinearGradient
            style={{
              marginTop: 5,
              paddingTop: 7,
              paddingBottom: 10,
              borderRadius: 2,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={['#38ee7d', '#11988d']}>
            <Text
              style={{
                fontSize: 18,
                color: '#FFFFFF',
                fontFamily: 'SourceSansPro-Semibold',
              }}>
              Cari Disposisi
            </Text>
          </LinearGradient>
        </TouchableOpacity>
      </View> */}

      {/* Modals  */}
      {/* Modal Search  */}
      <Modal
        animationType="fade"
        transparent
        visible={isModalSearchOpen}
        onRequestClose={() => toggleModalSearchOpen(false)}>
        <View style={styles.containerMain}>
          {/* Modal Head */}
          <View style={styles.containerHead}>
            <View style={styles.containerTitle}>
              <Text style={styles.textTitle}>Pencarian</Text>
              <TouchableOpacity onPress={() => toggleModalSearchOpen(false)}>
                <IcFontisto name="close-a" size={20} color="#424242" />
              </TouchableOpacity>
            </View>
          </View>

          {/* List Data Yang Dipilih User  */}
          <View style={{flex: 1, backgroundColor: '#fff'}}>
            <ScrollView contentContainerStyle={styles.containerBody}>
              {/* Input Keyword */}
              <View style={styles.containerInput}>
                <TextInput
                  placeholder="Ketik Pencarian di Sini..."
                  placeholderTextColor="#424242"
                  style={styles.textInput}
                  value={keyword}
                  onChangeText={setKeyword}
                />
              </View>

              {/* Date Start Picker  */}
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: 'OpenSans-Regular',
                  color: '#424242',
                  marginTop: 8,
                }}>
                Tanggal Awal
              </Text>
              <DatePicker1
                isShowDate={isShowDate1}
                value={date1}
                onChange={handledate1}
                stringDate={stringDate1}
                onPress={() => toggleShowDate1(true)}
              />

              {/* Date End Picker  */}
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: 'OpenSans-Regular',
                  color: '#424242',
                  marginTop: 8,
                }}>
                Tanggal Akhir
              </Text>
              <DatePicker2
                isShowDate={isShowDate2}
                value={date2}
                onChange={handledate2}
                stringDate={stringDate2}
                onPress={() => toggleShowDate2(true)}
              />

              {/* Filter Search  */}
              <Text
                style={{
                  fontSize: 15,
                  fontFamily: 'OpenSans-Regular',
                  color: '#424242',
                  marginTop: 8,
                }}>
                Pencarian Jenis Lain
              </Text>
              {/* isContentNumber */}
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  borderColor: '#eeeeee',
                  borderWidth: 1,
                  paddingTop: 6,
                  paddingBottom: 7,
                  paddingHorizontal: 15,
                  marginTop: 5,
                }}
                onPress={() => handleCheckFilterSearch('isContentNumber')}>
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: 'SourceSansPro-Semibold',
                    color: '#424242',
                  }}>
                  isContentNumber
                </Text>
                <CheckBox
                  value={isContentNumber}
                  onChange={() => handleCheckFilterSearch('isContentNumber')}
                />
              </TouchableOpacity>

              {/* isSubject */}
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  borderColor: '#eeeeee',
                  borderWidth: 1,
                  paddingTop: 6,
                  paddingBottom: 7,
                  paddingHorizontal: 15,
                  marginTop: 5,
                }}
                onPress={() => handleCheckFilterSearch('isSubject')}>
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: 'SourceSansPro-Semibold',
                    color: '#424242',
                  }}>
                  isSubject
                </Text>
                <CheckBox
                  value={isSubject}
                  onChange={() => handleCheckFilterSearch('isSubject')}
                />
              </TouchableOpacity>

              {/* isContentActivityCode */}
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  borderColor: '#eeeeee',
                  borderWidth: 1,
                  paddingTop: 6,
                  paddingBottom: 7,
                  paddingHorizontal: 15,
                  marginTop: 5,
                }}
                onPress={() =>
                  handleCheckFilterSearch('isContentActivityCode')
                }>
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: 'SourceSansPro-Semibold',
                    color: '#424242',
                  }}>
                  isContentActivityCode
                </Text>
                <CheckBox
                  value={isContentActivityCode}
                  onChange={() =>
                    handleCheckFilterSearch('isContentActivityCode')
                  }
                />
              </TouchableOpacity>

              {/* isContentSender */}
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  borderColor: '#eeeeee',
                  borderWidth: 1,
                  paddingTop: 6,
                  paddingBottom: 7,
                  paddingHorizontal: 15,
                  marginTop: 5,
                }}
                onPress={() => handleCheckFilterSearch('isContentSender')}>
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: 'SourceSansPro-Semibold',
                    color: '#424242',
                  }}>
                  isContentSender
                </Text>
                <CheckBox
                  value={isContentSender}
                  onChange={() => handleCheckFilterSearch('isContentSender')}
                />
              </TouchableOpacity>

              {/* isSender */}
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  borderColor: '#eeeeee',
                  borderWidth: 1,
                  paddingTop: 6,
                  paddingBottom: 7,
                  paddingHorizontal: 15,
                  marginTop: 5,
                }}
                onPress={() => handleCheckFilterSearch('isSender')}>
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: 'SourceSansPro-Semibold',
                    color: '#424242',
                  }}>
                  isSender
                </Text>
                <CheckBox
                  value={isSender}
                  onChange={() => handleCheckFilterSearch('isSender')}
                />
              </TouchableOpacity>

              {/* isRecipient */}
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                  justifyContent: 'space-between',
                  borderColor: '#eeeeee',
                  borderWidth: 1,
                  paddingTop: 6,
                  paddingBottom: 7,
                  paddingHorizontal: 15,
                  marginTop: 5,
                }}
                onPress={() => handleCheckFilterSearch('isRecipient')}>
                <Text
                  style={{
                    fontSize: 16,
                    fontFamily: 'SourceSansPro-Semibold',
                    color: '#424242',
                  }}>
                  isRecipient
                </Text>
                <CheckBox
                  value={isRecipient}
                  onChange={() => handleCheckFilterSearch('isRecipient')}
                />
              </TouchableOpacity>
            </ScrollView>
            <View
              style={{
                paddingBottom: 5,
                paddingHorizontal: 16,
              }}>
              {/* Button Reset CheckBox to false */}
              <TouchableOpacity
                onPress={resetSearch}
                style={{
                  paddingTop: 7,
                  paddingBottom: 10,
                  borderRadius: 2,
                  borderWidth: 1,
                  borderColor: '#9e9e9e',
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginBottom: 10,
                }}>
                <Text
                  style={{
                    fontSize: 18,
                    color: '#616161',
                    fontFamily: 'SourceSansPro-Semibold',
                  }}>
                  Reset
                </Text>
              </TouchableOpacity>

              {/* Button Terapkan */}
              <TouchableOpacity onPress={applySearch}>
                <LinearGradient
                  style={{
                    marginBottom: 27,
                    paddingTop: 7,
                    paddingBottom: 10,
                    borderRadius: 2,
                    width: '100%',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  start={{x: 0, y: 0}}
                  end={{x: 1, y: 0}}
                  colors={['#38ee7d', '#11988d']}>
                  <Text
                    style={{
                      fontSize: 18,
                      color: '#FFFFFF',
                      fontFamily: 'SourceSansPro-Semibold',
                    }}>
                    Terapkan
                  </Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>

      {/* Modal Filter  */}
      <Modal
        animationType="fade"
        transparent
        visible={isModalFilterOpen}
        onRequestClose={() => toggleModalFilter(false)}>
        {/* Container Modal  */}
        <TouchableOpacity
          onPress={() => toggleModalFilter(false)}
          style={{flex: 1, backgroundColor: '#000000', opacity: 0.6}}
        />
        <View
          style={{
            paddingHorizontal: 20,
            paddingVertical: 15,
            width: Dimensions.get('window').width - 30,
            position: 'absolute',
            top: Dimensions.get('window').width * 0.45,
            borderRadius: 2,
            alignSelf: 'center',
            backgroundColor: '#FFFFFF',
          }}>
          {/* Isi Modal */}
          {/* Semua  */}
          <TouchableOpacity
            style={{
              marginTop: 5,
              paddingVertical: 7,
            }}
            onPress={() => handleIndependentFilter('semua')}>
            <Text
              style={{
                fontSize: 16,
                fontFamily: 'OpenSans-SemiBold',
                color: '#424242',
              }}>
              Semua Disposisi
            </Text>
          </TouchableOpacity>
          {/* isRead  */}
          <TouchableOpacity
            style={{marginTop: 5, paddingVertical: 7}}
            onPress={() => handleIndependentFilter('isRead')}>
            <Text
              style={{
                fontSize: 16,
                fontFamily: 'OpenSans-SemiBold',
                color: '#424242',
              }}>
              Sudah Dibaca
            </Text>
          </TouchableOpacity>
          {/* isUnread  */}
          <TouchableOpacity
            style={{marginTop: 5, paddingVertical: 7}}
            onPress={() => handleIndependentFilter('isUnread')}>
            <Text
              style={{
                fontSize: 16,
                fontFamily: 'OpenSans-SemiBold',
                color: '#424242',
              }}>
              Belum Dibaca
            </Text>
          </TouchableOpacity>
          {/* isReported  */}
          <TouchableOpacity
            style={{marginTop: 5, paddingVertical: 7}}
            onPress={() => handleIndependentFilter('isReported')}>
            <Text
              style={{
                fontSize: 16,
                fontFamily: 'OpenSans-SemiBold',
                color: '#424242',
              }}>
              Dilaporkan
            </Text>
          </TouchableOpacity>
          {/* isForwarded  */}
          <TouchableOpacity
            style={{marginTop: 5, paddingVertical: 7}}
            onPress={() => handleIndependentFilter('isForwarded')}>
            <Text
              style={{
                fontSize: 16,
                fontFamily: 'OpenSans-SemiBold',
                color: '#424242',
              }}>
              Diteruskan
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    </>
  );
};

ListInformasi.navigationOptions = ({navigation: {getParam, goBack}}) => ({
  headerTitle: getParam('title'),
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="back"
        color="#424242"
        iconName="arrow-left"
        onPress={() => goBack()}
      />
    </NavHeaderButtons>
  ),
  headerRight: (
    <NavHeaderButtons>
      <Item
        title="filter"
        color="#424242"
        iconName="filter"
        onPress={getParam('openFilter')}
      />
      <Item
        title="search"
        color="#424242"
        iconName="search"
        onPress={getParam('openSearch')}
      />
    </NavHeaderButtons>
  ),
});

const styles = StyleSheet.create({
  containerMain: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    backgroundColor: 'transparent',
  },
  containerHead: {
    backgroundColor: '#fff',
    paddingHorizontal: 16,
    paddingTop: 20,
    paddingBottom: 15,
  },
  containerTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  containerInput: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 3,
    borderWidth: 1,
    borderColor: '#bdbdbd',
    marginTop: 13,
  },
  containerBody: {
    paddingTop: 5,
    paddingBottom: 100,
    paddingHorizontal: 16,
  },
  textTitle: {
    fontSize: 20,
    fontFamily: 'OpenSans-SemiBold',
    color: '#424242',
  },
  textInput: {
    flex: 1,
    fontSize: 15,
    fontFamily: 'OpenSans-Regular',
    color: '#424242',
    marginLeft: 16,
    paddingVertical: 8,
  },
});

export default ListInformasi;
