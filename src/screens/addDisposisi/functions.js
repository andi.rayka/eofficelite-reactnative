import AsyncStorage from '@react-native-community/async-storage';
import {apiMaster} from 'rootapp/src/services/api';
import {addCheckedValueToObj} from 'rootapp/src/commonFunctions';

export const getCommands = async functName => {
  const token = await AsyncStorage.getItem('mainLoginToken');

  //contentActivityType-> 1 = disposisi, 2 = informasi
  await apiMaster
    .post('/getUserCommands', {
      token,
      contentActivityType: 1,
    })
    .then(resp => {
      const {code, error_message} = resp.data.system;
      const {data} = resp.data;

      const hasil = addCheckedValueToObj(data);

      if (code === 409) {
        functName(hasil);
        console.log('commands: ', hasil);
      } else {
        alert(error_message);
      }
    })
    .catch(error => {
      alert('Gagal mengambil data');
      console.log(error);
    });
};

// Get ReceiverList
export const getReceiverList = async (keyword, funcName) => {
  const token = await AsyncStorage.getItem('mainLoginToken');

  await apiMaster
    .post('/getDispositionReceiverIntList', {
      token,
      keyword,
    })
    .then(resp => {
      const {code, error_message} = resp.data.system;
      const {data} = resp.data;

      const finalValue = addCheckedValueToObj(data);

      if (code === 410) {
        funcName(finalValue);
        console.log('receiver: ', data);
      } else {
        alert(error_message);
      }
    })
    .catch(error => {
      alert('Gagal mengambil data penerima informasi');
      console.log(error);
    });
};
