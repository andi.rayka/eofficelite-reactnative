import React from 'react';
import {View, TouchableOpacity, Text, StyleSheet} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

const MultiChoiceBox = ({data, onChoose}) => {
  // Component yang isChecked == false
  const ItemNotChecked = ({text, onPress}) => {
    return (
      <TouchableOpacity style={styles.containerNotChecked} onPress={onPress}>
        <Text style={styles.textNotChecked}>{text}</Text>
      </TouchableOpacity>
    );
  };

  // Component yang isChecked == true
  const ItemChecked = ({text, onPress}) => {
    return (
      <TouchableOpacity onPress={onPress}>
        <LinearGradient
          colors={['#38ee7d', '#11988d']}
          style={styles.containerChecked}>
          <Text style={styles.textChecked}>{text}</Text>
        </LinearGradient>
      </TouchableOpacity>
    );
  };

  // Handle Pilih Item
  const handleChoose = id => {
    let arrayId = [];
    let newDataForParent = [];

    // Update data terpilih jadi punya value sebaliknya -> true/false
    let objId = data.findIndex(obj => obj.idca_trait === id);
    let objBaru = data[objId];
    objBaru = {...objBaru, isChecked: !objBaru.isChecked};
    let newDispositionTraitData = Object.assign([...data], {
      [objId]: objBaru,
    });

    // Pass id yang isChecked === true
    for (let i = 0; i < newDispositionTraitData.length; i++) {
      const item = newDispositionTraitData[i];
      if (item.isChecked) {
        arrayId = [...arrayId, item.idca_trait];
      }
    }

    newDataForParent = newDispositionTraitData;

    // onChoose(arrayId, newDataForParent);
  };

  return (
    <View style={{marginTop: 10}}>
      <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
        {data.map((item, key) => {
          let text = item.catrait_name,
            id = item.idca_trait;

          if (item.isChecked) {
            return (
              <ItemChecked key={key} text={text} onPress={() => onChoose(id)} />
            );
          } else {
            return (
              <ItemNotChecked
                key={key}
                text={text}
                onPress={() => onChoose(id)}
              />
            );
          }
        })}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  containerChecked: {
    paddingHorizontal: 11,
    paddingVertical: 8,
    borderRadius: 3,
    marginRight: 9,
    marginTop: 5,
  },
  containerNotChecked: {
    backgroundColor: '#ffffff',
    borderWidth: 1,
    borderColor: '#38ee7d',
    paddingHorizontal: 11,
    paddingVertical: 8,
    borderRadius: 3,
    marginRight: 9,
    marginTop: 5,
  },
  textInstruksi: {
    fontSize: 14,
    fontFamily: 'OpenSans-Regular',
    color: '#424242',
  },
  textChecked: {
    fontSize: 14,
    fontFamily: 'OpenSans-Regular',
    color: '#ffffff',
  },
  textNotChecked: {
    fontSize: 14,
    fontFamily: 'OpenSans-Regular',
    color: '#424242',
  },
});

export default MultiChoiceBox;
