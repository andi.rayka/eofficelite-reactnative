import React from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  Modal,
  Dimensions,
  ScrollView,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient/';
import IcFontisto from 'react-native-vector-icons/Fontisto';

const ModalPenerima = ({
  isModalOpen,
  closeModal,
  data,
  onChangeText,
  onChoose,
}) => {
  const RenderItem = ({name, position, unit, isChecked, rec_int_text}) => {
    return (
      <TouchableOpacity
        style={{
          backgroundColor: isChecked ? 'grey' : '#fff',
          borderRadius: 3,
          padding: 15,
          marginTop: 13,
          flexDirection: 'row',
          elevation: 3,
        }}
        onPress={() => onChoose(rec_int_text)}>
        <View
          style={{
            borderRadius: 50,
            backgroundColor: 'purple',
            width: 35,
            height: 35,
          }}
        />
        <View style={{paddingHorizontal: 5, marginLeft: 5}}>
          <Text
            style={{
              fontSize: 15,
              fontFamily: 'OpenSans-SemiBold',
              color: '#212121',
            }}>
            {name}
          </Text>
          <Text
            style={{
              fontSize: 15,
              fontFamily: 'SourceSansPro-Regular',
              color: '#212121',
            }}>
            {position}
          </Text>
          <Text
            style={{
              fontSize: 14,
              fontFamily: 'SourceSansPro-Regular',
              color: '#616161',
            }}>
            {unit}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <Modal
      animationType="fade"
      transparent
      visible={isModalOpen}
      onRequestClose={closeModal}>
      <View style={styles.containerMain}>
        {/* Modal Head */}
        <View style={styles.containerHead}>
          <View style={styles.containerTitle}>
            <Text style={styles.textTitle}>List Kontak Internal</Text>
            <TouchableOpacity onPress={closeModal}>
              <IcFontisto name="close-a" size={20} color="#424242" />
            </TouchableOpacity>
          </View>

          {/* Input Keyword */}
          <View style={styles.containerInput}>
            <TextInput
              placeholder="Cari Nama Penerima"
              placeholderTextColor="#424242"
              style={styles.textInput}
              onChangeText={onChangeText}
            />
            <TouchableOpacity>
              <LinearGradient
                colors={['#38ee7d', '#11988d']}
                style={{paddingVertical: 12, paddingHorizontal: 13}}>
                <IcFontisto name="search" size={20} color="#fff" />
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>

        {/* List Data Yang Dipilih User  */}
        <View style={{flex: 1, backgroundColor: '#f5f5f5'}}>
          <ScrollView contentContainerStyle={styles.containerBody}>
            {data.map((item, key) => {
              return (
                <RenderItem
                  key={key}
                  name={item.usname}
                  position={item.positionname}
                  unit={item.nameunit}
                  isChecked={item.isChecked}
                  rec_int_text={item.rec_int_text}
                />
              );
            })}
          </ScrollView>
          <TouchableOpacity onPress={closeModal}>
            <LinearGradient
              style={{
                marginTop: 15,
                marginBottom: 27,
                paddingTop: 7,
                paddingBottom: 10,
                borderRadius: 2,
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
              }}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              colors={['#38ee7d', '#11988d']}>
              <Text
                style={{
                  fontSize: 18,
                  color: '#FFFFFF',
                  fontFamily: 'SourceSansPro-Semibold',
                }}>
                Terapkan
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  containerMain: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    backgroundColor: 'transparent',
  },
  containerHead: {
    backgroundColor: '#fff',
    paddingHorizontal: 16,
    paddingTop: 20,
    paddingBottom: 20,
    elevation: 3,
  },
  containerTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 22,
  },
  containerInput: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 3,
    borderWidth: 1,
    borderColor: '#38ee7d',
    marginTop: 13,
  },
  containerBody: {
    paddingTop: 5,
    paddingBottom: 100,
    paddingHorizontal: 16,
  },
  textTitle: {
    fontSize: 20,
    fontFamily: 'OpenSans-SemiBold',
    color: '#424242',
  },
  textInput: {
    flex: 1,
    fontSize: 15,
    fontFamily: 'OpenSans-SemiBold',
    color: '#424242',
    marginLeft: 16,
    paddingVertical: 8,
  },
});

export default ModalPenerima;
