import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Modal,
  StyleSheet,
  Dimensions,
  ImageBackground,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  Button,
  CheckBox,
} from 'react-native';

import {apiActivity, apiMaster2Param} from 'rootapp/src/services/api';
import AsyncStorage from '@react-native-community/async-storage';
import {getCommands, getReceiverList} from './functions';

import Icon from 'react-native-vector-icons/Fontisto';
import IconAwesome from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient/';
import {
  NavHeaderButtons,
  Item,
} from 'rootapp/src/components/navigation/HeaderButton';
import DatePicker from '@react-native-community/datetimepicker';

import KartuDisposisi from './KartuDisposisi';
import MultiChoiceBox from './MultiChoiceBox';
import PickListData from 'rootapp/src/components/form/PickListData';
import RegularForm from 'rootapp/src/components/form/SingleLine';
import ModalPenerima from './ModalPenerima';

const AddDisposisi = ({navigation: {getParam, setParams, navigate}}) => {
  const [isPrivate, togglePrivate] = useState(false);
  const [isNeedReport, toggleIsNeedReport] = useState(false);

  // Expired Date
  const [isShowDate, toggleShowDate] = useState(false);
  const [expiredDate, setExpiredDate] = useState(new Date());
  const [stringDate, setStringDate] = useState('Batas Akhir Disposisi');

  const [fetchedDispositionTraits, setFetchedDispositionTraits] = useState([]);
  const [fetchedCommands, setFetchedCommands] = useState([]);
  const [notes, setNotes] = useState('');

  // Receiver
  const [receiverKeyword, setReceiverKeyword] = useState('');
  const [fetchedReceiver, setFetchedReceiver] = useState([]);

  // Modal
  const [isModalCommandsOpen, toggleModalCommands] = useState(false);
  const [isModalReceiverOpen, toggleModalReceiver] = useState(false);

  const contentDetail = getParam('contentDetail');

  // Initial Fetch
  // Get Commands, Disposition Traits
  useEffect(() => {
    getCommands(setFetchedCommands);
    apiMaster2Param(
      '/getContentActivityTraitList',
      413,
      setFetchedDispositionTraits,
    );
  }, []);

  // Get Receiver/Internal Contact List
  useEffect(() => {
    getReceiverList(receiverKeyword, setFetchedReceiver);
  }, [receiverKeyword]);
  // Add Disposisi
  const kirimDisposisi = async () => {
    const token = await AsyncStorage.getItem('mainLoginToken');

    // Pass checked item
    let arrayIdCommands = [];
    for (let i = 0; i < fetchedCommands.length; i++) {
      const item = fetchedCommands[i];
      if (item.isChecked) {
        arrayIdCommands = [...arrayIdCommands, item.idcommand];
      }
    }

    // Pass checked item
    let arrayTraits = [];
    for (let i = 0; i < fetchedDispositionTraits.length; i++) {
      const item = fetchedDispositionTraits[i];
      if (item.isChecked) {
        arrayTraits = [...arrayTraits, item.idcommand];
      }
    }

    // buat array Receiver berisi iduser, idposition, idunit, rec_int_text yang dipilih
    let arrayReceiver = [];
    for (let i = 0; i < fetchedReceiver.length; i++) {
      const item = fetchedReceiver[i];
      if (item.isChecked) {
        let valueBaru = {
          iduser: item.iduser,
          idposition: item.idposition,
          idunit: item.idunit,
          textint_recipient: item.rec_int_text,
        };
        arrayReceiver = [...arrayReceiver, valueBaru];
      }
    }

    console.log('idCommand', arrayIdCommands);
    console.log('trait semua', fetchedDispositionTraits);
    console.log('arrTraits', arrayTraits);
    console.log('arrReceipt', arrayReceiver);

    await apiActivity
      .post('/addDispositionFromContent', {
        token,
        idCommands: arrayIdCommands,
        idContentActivityTraits: ['55085ef0-c478-432d-9694-eb520456db73'],
        idContent: contentDetail.idcontent,
        isNeedReport: +isNeedReport,
        isPrivate: +isPrivate,
        notes,
        expiredDate: stringDate,
        recipients: arrayReceiver,
      })
      .then(resp => {
        const {code, error_message} = resp.data.system;

        if (code === 236) {
          navigate('Home');
          alert('Berhasil Membuat Disposisi');
        } else {
          alert(error_message);
        }
      })
      .catch(error => {
        alert('Gagal mengolah data');
        console.log(error);
      });
  };

  // Handle Pilih Sifat Disposisi
  const handleChoicefetchedDispositionTrait = id => {
    console.log(fetchedDispositionTraits);
    let objId = fetchedDispositionTraits.findIndex(
      obj => obj.idca_trait === id,
    );

    let valueBaru = fetchedDispositionTraits[objId];
    valueBaru = {...valueBaru, isChecked: !valueBaru.isChecked};

    let objBaru = Object.assign([...fetchedDispositionTraits], {
      [objId]: valueBaru,
    });

    setFetchedDispositionTraits(objBaru);
  };

  // Handle pilih tanggal tenggat disposisi
  const handleDate = (event, pickedDate) => {
    toggleShowDate(false);

    if (pickedDate) {
      let dd = String(pickedDate.getDate()).padStart(2, '0');
      let mm = String(pickedDate.getMonth() + 1).padStart(2, '0'); //January is 0!
      let yyyy = pickedDate.getFullYear();

      let fullDate = dd + '-' + mm + '-' + yyyy;

      setExpiredDate(pickedDate);
      setStringDate(fullDate);
    }
  };

  // Handle pilih commands/instruksi
  const handleCommandChoice = id => {
    //   Mengupdate isChecked
    let objId = fetchedCommands.findIndex(obj => obj.idcommand === id);

    let valueBaru = fetchedCommands[objId];
    valueBaru = {...valueBaru, isChecked: !valueBaru.isChecked};

    let objBaru = Object.assign([...fetchedCommands], {
      [objId]: valueBaru,
    });

    setFetchedCommands(objBaru);
  };

  // Handle pilih penerima Disposisi
  const handleChooseReceiver = text => {
    // Update data terpilih jadi punya value sebaliknya -> true/false
    let objId = fetchedReceiver.findIndex(obj => obj.rec_int_text === text);

    let objBaru = fetchedReceiver[objId];
    objBaru = {...objBaru, isChecked: !objBaru.isChecked};

    let newData = Object.assign([...fetchedReceiver], {[objId]: objBaru});

    setFetchedReceiver(newData);
  };

  return (
    <ScrollView style={{flex: 1}}>
      <StatusBar backgroundColor="#34E27E" />

      {/* Header Halaman */}
      <ImageBackground
        source={{uri: 'bg_home'}}
        resizeMode="cover"
        style={{paddingBottom: 0}}>
        <KartuDisposisi
          contentnumber={contentDetail.contentnumber}
          contentspeed_name={contentDetail.contentspeed_name}
          subject={contentDetail.subject}
          summary={contentDetail.summary}
        />
      </ImageBackground>

      {/* Body halaman */}
      <View style={{paddingHorizontal: 15, paddingTop: 14}}>
        {/* Toggle isPrivate  */}
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginLeft: -4,
          }}
          onPress={() => togglePrivate(!isPrivate)}>
          <CheckBox
            value={isPrivate}
            onValueChange={() => togglePrivate(!isPrivate)}
          />
          <Text>Disposisi Private</Text>
        </TouchableOpacity>

        {/* Toggle Wajib Dilaporkan  */}
        <TouchableOpacity
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginLeft: -4,
          }}
          onPress={() => toggleIsNeedReport(!isNeedReport)}>
          <CheckBox
            value={isNeedReport}
            onValueChange={() => toggleIsNeedReport(!isNeedReport)}
          />
          <Text>Wajib Dilaporkan</Text>
        </TouchableOpacity>

        {/* Pilih Sifat Disposisi  */}
        <MultiChoiceBox
          data={fetchedDispositionTraits}
          onChoose={handleChoicefetchedDispositionTrait}
        />

        {/* Pilih tanggal tenggat disposisi */}
        {isShowDate && (
          <DatePicker mode="date" value={expiredDate} onChange={handleDate} />
        )}

        <TouchableOpacity
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginTop: 10,
            borderWidth: 1,
            borderColor: '#eeeeee',
            borderRadius: 3,
            paddingHorizontal: 17,
            paddingVertical: 9,
          }}
          onPress={() => toggleShowDate(true)}>
          <Icon name="date" style={{fontSize: 25}} />
          <Text
            style={{
              fontSize: 14,
              fontFamily: 'OpenSans-SemiBold',
              color: '#424242',
              marginLeft: 15,
            }}>
            {stringDate}
          </Text>
        </TouchableOpacity>

        {/* Pilih Commands  */}
        <PickListData
          onPress={() => toggleModalCommands(true)}
          text="Pilih Instruksi"
        />

        {fetchedCommands.map((item, key) => {
          if (item.isChecked) {
            return (
              <View key={key} style={styles.itemTerpilih}>
                <Text style={{fontSize: 15}}>> {item.command_name}</Text>
              </View>
            );
          }
        })}

        {/* Pilih Penerima Disposisi */}
        <PickListData
          onPress={() => toggleModalReceiver(true)}
          text="Pilih Penerima Disposisi"
        />
        {fetchedReceiver.map((item, key) => {
          if (item.isChecked) {
            return (
              <View key={key} style={styles.itemTerpilih}>
                <Text style={{fontSize: 15}}>
                  {item.positionname} - {item.nameunit}
                </Text>
              </View>
            );
          }
        })}

        {/* Input Catatan  */}
        <RegularForm
          placeholder="Catatan Lainnya"
          value={notes}
          onChange={setNotes}
        />

        <TouchableOpacity onPress={kirimDisposisi}>
          <LinearGradient
            style={styles.containerDuaTombolPalingBawah}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={['#38ee7d', '#11988d']}>
            <Text style={styles.textTerapkan}>Kirim Disposisi</Text>
          </LinearGradient>
        </TouchableOpacity>

        <View style={{marginTop: 50}} />
      </View>

      {/* Modals  */}
      {/* Modal Commands */}
      <Modal
        animationType="fade"
        transparent
        visible={isModalCommandsOpen}
        onRequestClose={() => toggleModalCommands(false)}>
        <TouchableOpacity
          onPress={() => toggleModalCommands(false)}
          style={styles.containerFreespace}
        />

        <View style={styles.containerModalContent}>
          <View style={styles.containerJudulModal}>
            <Text style={styles.textJudulSection}>Pilih instruksi</Text>
            <TouchableOpacity onPress={() => toggleModalCommands(false)}>
              <IconAwesome name="times-circle" style={styles.iconClose} />
            </TouchableOpacity>
          </View>

          {fetchedCommands.map((item, key) => {
            return (
              <CheckItem
                key={key}
                value={item.isChecked}
                text={item.command_name}
                onChange={() => handleCommandChoice(item.idcommand)}
              />
            );
          })}

          <TouchableOpacity onPress={() => toggleModalCommands(false)}>
            <LinearGradient
              style={styles.containerDuaTombolPalingBawah}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              colors={['#38ee7d', '#11988d']}>
              <Text style={styles.textTerapkan}>Terapkan</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </Modal>

      {/* Modal Penerima  */}
      <ModalPenerima
        isModalOpen={isModalReceiverOpen}
        closeModal={() => toggleModalReceiver(false)}
        data={fetchedReceiver}
        onChangeText={setReceiverKeyword}
        onChoose={handleChooseReceiver}
      />
      <View style={{marginVertical: 20}} />
    </ScrollView>
  );
};

const CheckItem = ({value, text, onChange}) => {
  return (
    <TouchableOpacity
      onPress={onChange}
      style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
      <CheckBox value={value} onValueChange={onChange} />
      <Text>{text}</Text>
    </TouchableOpacity>
  );
};

AddDisposisi.navigationOptions = ({navigation: {getParam, goBack}}) => ({
  headerTitle: 'Buat Disposisi',
  headerStyle: {
    backgroundColor: '#34E27E',
    elevation: 0,
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 22,
    marginLeft: 0,
  },
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="back"
        color="#fff"
        iconName="arrow-left"
        onPress={() => goBack()}
      />
    </NavHeaderButtons>
  ),
  //   headerRight: (
  //     <NavHeaderButtons>
  //       <Item
  //         title="send"
  //         color="#fff"
  //         iconName="paper-plane"
  //         onPress={getParam('addDisposition')}
  //       />
  //     </NavHeaderButtons>
  //   ),
});

const styles = StyleSheet.create({
  // baru
  itemTerpilih: {
    padding: 8,
    marginTop: 7,
    backgroundColor: '#d3d3d3',
    borderRadius: 3,
  },
  //
  containerFreespace: {
    flex: 1,
    backgroundColor: '#000000',
    opacity: 0.6,
  },
  containerModalContent: {
    width: Dimensions.get('window').width,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 19,
    paddingVertical: 15,
  },
  containerJudulModal: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  containerOptionFilter: {
    marginTop: 15,
  },
  containerDuaTombolPalingBawah: {
    marginTop: 15,
    paddingTop: 7,
    paddingBottom: 10,
    borderRadius: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconClose: {
    fontSize: 25,
  },
  inputPencarian: {
    borderWidth: 1,
    borderColor: '#eeeeee',
    borderRadius: 3,
    paddingHorizontal: 15,
    paddingVertical: 8,
    fontSize: 14,
    fontFamily: 'OpenSans',
    marginTop: 10,
  },
  textJudulSection: {
    fontSize: 16,
    color: '#424242',
    fontFamily: 'OpenSans-Semibold',
    marginVertical: 6,
  },
  textTerapkan: {
    fontSize: 18,
    color: '#FFFFFF',
    fontFamily: 'SourceSansPro-Semibold',
  },
});

export default AddDisposisi;
