import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import {apiActivity} from 'rootapp/src/services/api';

import {
  NavHeaderButtons,
  Item,
} from 'rootapp/src/components/navigation/HeaderButton';
import LoadingScreen from 'rootapp/src/components/common/LoadingScreen';
import KopSurat from './KopSurat';
import KopDisposisi from './KopDisposisi';
import PenerimaSurat from './PenerimaSurat';
import PenerimaDisposisi from './PenerimaDisposisi';
import Lampiran from './Lampiran';

const DetailDisposisiMasuk = ({navigation: {navigate, getParam}}) => {
  const [fetchedDisposition, setFetchedDisposition] = useState();
  const [fetchedContent, setFetchedContent] = useState();
  const [isLoading, setLoading] = useState(true);

  // Initial Fetch
  useEffect(() => {
    const getDetailSurat = async () => {
      const idcontentactivity = getParam('idcontentactivity');
      const token = await AsyncStorage.getItem('mainLoginToken');

      await apiActivity
        .post('/getDispositionDetail', {
          token,
          idDisposition: idcontentactivity,
        })
        .then(resp => {
          const {code, error_message} = resp.data.system;
          const {disposition, content} = resp.data.data;

          if (code === 239) {
            setFetchedDisposition(disposition);
            setFetchedContent(content);
            setLoading(false);
            console.log('content', content);
          } else {
            alert(error_message);
          }
        })
        .catch(error => {
          alert('Gagal mengambil data');
          console.log(error);
        });
    };

    getDetailSurat();
  }, [getParam]);

  return isLoading ? (
    <LoadingScreen />
  ) : (
    <ScrollView>
      <View style={styles.bgPutih}>
        <KopDisposisi
          contentnumber={fetchedDisposition.contentactivitycode}
          subject={fetchedDisposition.commands_text}
          summary={fetchedDisposition.notes}
          status={fetchedDisposition.castatus_name}
        />

        <PenerimaDisposisi
          textext_act={fetchedDisposition.textint_act}
          // unit_name={unit_name}
          recipients={fetchedDisposition.contentactivityrecipientlist}
        />
      </View>

      {/* Detail Surat  */}

      <Text
        style={{
          marginTop: 13,
          marginLeft: 10,
          fontSize: 22,
          color: '#212121',
          fontFamily: 'OpenSans-SemiBold',
        }}>
        Detail Surat Terkait
      </Text>
      <View style={styles.bgPutih}>
        <KopSurat
          contentnumber={fetchedContent.contentnumber}
          subject={fetchedContent.subject}
          summary={fetchedContent.summary}
          status={fetchedContent.contenttrait_name}
        />

        <PenerimaSurat
          textext_act={fetchedContent.textext_act}
          unit_name={fetchedContent.unit_name}
          recipients={fetchedContent.recipients}
        />

        <Lampiran attachments={fetchedContent.attachments} />
      </View>
    </ScrollView>
  );
};

DetailDisposisiMasuk.navigationOptions = ({
  navigation: {goBack, getParam},
}) => ({
  headerTitle: 'Detail Disposisi',
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="back"
        color="#424242"
        iconName="arrow-left"
        onPress={() => goBack()}
      />
    </NavHeaderButtons>
  ),
});

const styles = StyleSheet.create({
  bgPutih: {
    backgroundColor: '#FFFFFF',
    paddingVertical: 10,
  },
  bgAbu: {
    backgroundColor: '#f5f5f5',
    paddingTop: 15,
    paddingBottom: 180,
    paddingLeft: 15,
    paddingRight: 17,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buttonKirim: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#37d67a',
    borderRadius: 3,
    paddingTop: 7,
    paddingBottom: 6,
    paddingLeft: 18,
    flex: 1,
    backgroundColor: '#ffffff',
  },
  textButton: {
    fontSize: 15,
    color: '#424242',
    fontFamily: 'OpenSans-SemiBold',
  },
});

export default DetailDisposisiMasuk;
