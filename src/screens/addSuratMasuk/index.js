import React, {useState, useEffect} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  StatusBar,
  Button,
  CheckBox,
} from 'react-native';

import {apiContent, apiMaster2Param} from 'rootapp/src/services/api';
import {getInternalContact, getTopicOrExt} from './functions';
import AsyncStorage from '@react-native-community/async-storage';

import {
  NavHeaderButtons,
  Item,
} from 'rootapp/src/components/navigation/HeaderButton';
import LinearGradient from 'react-native-linear-gradient/';

import FormTraitSurat from './FormTraitSurat';
import RegularForm from 'rootapp/src/components/form/SingleLine';
import DatePicker from 'rootapp/src/components/form/DatePicker';
import PickListData from 'rootapp/src/components/form/PickListData';
import ModalSpeed from './ModalSpeed';
import ModalPengirim from './ModalPengirim';
import ModalPenerima from './ModalPenerima';
import ModalCc from './ModalPenerima';
import ModalTopik from './ModalTopik';
import ModalUnit from './ModalUnit';

const AddSuratMasuk = ({navigation: {goBack}}) => {
  // Lainnya
  const [isPrivate, togglePrivate] = useState(false);
  const [noSurat, setNoSurat] = useState('');
  const [perihal, setPerihal] = useState('');
  const [ringkasan, setRingkasan] = useState('');
  const [alamat, setAlamat] = useState('');
  const [trait, setTrait] = useState('b7f89d08-f9bc-4fe7-94c3-f21dcbda9b58');

  // Date
  const [contentDate, setContentDate] = useState(new Date());
  const [stringDate, setStringDate] = useState('Tanggal Surat');
  const [isShowDate, toggleShowDate] = useState(false);

  // Unit
  const [idUnit, setIdUnit] = useState('');
  const [textUnit, setTextUnit] = useState('Pilih Unit Surat');
  const [fetchedUnit, setFetchedUnit] = useState([]);

  // Speed
  const [idSpeed, setIdSpeed] = useState('');
  const [textSpeed, setTextSpeed] = useState('Pilih Speed Surat');
  const [fetchedSpeed, setFetchedSpeed] = useState([]);

  // Topic
  const [idTopic, setIdTopic] = useState('');
  const [textTopic, setTextTopic] = useState('Pilih Topik Surat');
  const [fetchedTopic, setFetchedTopic] = useState([]);
  const [topicKeyword, setTopicKeyword] = useState(false);

  // External Contact
  const [idExt, setIdExt] = useState('');
  const [textExt, setTextExt] = useState('');
  const [fetchedExt, setFetchedExt] = useState([]);
  const [extKeyword, setExtKeyword] = useState(false);
  const [newExtSave, setNewExtSave] = useState('');

  // Internal Contact
  const [fetchedInt, setFetchedInt] = useState([]);
  const [intKeyword, setIntKeyword] = useState(false);
  const [arrayInt, setArrayInt] = useState([]);

  // CarbonCopies
  const [fetchedCc, setFetchedCc] = useState([]);
  const [arrayCc, setArrayCc] = useState([]);

  // Toggle Modal
  const [isModalOpen, toggleModal] = useState({
    unit: false,
    speed: false,
    topic: false,
    ext: false,
    int: false,
    cc: false,
  });

  // Get Content Speed dan Unit
  useEffect(() => {
    apiMaster2Param('/getContentSpeed', 407, setFetchedSpeed);
    apiMaster2Param('/getUnitContentList', 408, setFetchedUnit);
  }, []);

  // Get Internal Contact List
  useEffect(() => {
    getInternalContact(intKeyword, setFetchedInt, setFetchedCc);
  }, [intKeyword]);

  // Get External Contact List
  useEffect(() => {
    getTopicOrExt('/getExternalContact', extKeyword, 401, setFetchedExt);
  }, [extKeyword]);

  // Get Topic List
  useEffect(() => {
    getTopicOrExt('/getContentTopic', topicKeyword, 402, setFetchedTopic);
  }, [topicKeyword]);

  // Add Content
  const addContent = async () => {
    const token = await AsyncStorage.getItem('mainLoginToken');

    // Pass checked item
    let arrInt = [];
    let arrCc = [];

    for (let i = 0; i < fetchedInt.length; i++) {
      const item = fetchedInt[i];
      if (item.isChecked) {
        let valueBaru = {
          iduser: item.iduser,
          idposition: item.idposition,
          idunit: item.idunit,
          textint_recipient: item.rec_int_text,
        };
        arrInt = [...arrInt, valueBaru];
      }
    }

    for (let i = 0; i < fetchedCc.length; i++) {
      const item = fetchedCc[i];
      if (item.isChecked) {
        let valueBaru = {
          iduser: item.iduser,
          idposition: item.idposition,
          idunit: item.idunit,
          textint_recipient: item.rec_int_text,
        };
        arrInt = [...arrInt, valueBaru];
      }
    }

    console.log('~~~~Check State Sebelum Post~~~~');
    console.log(stringDate);
    console.log(noSurat);
    console.log(perihal);
    console.log(ringkasan);
    console.log(alamat);
    console.log(idUnit);
    console.log(idSpeed);
    console.log(idTopic);
    console.log(trait);
    console.log(idExt);
    console.log(textExt);
    console.log(+isPrivate);
    console.log(arrInt);
    console.log(arrCc);

    await apiContent
      .post('/addContentIn', {
        token,
        contentDate: stringDate,
        contentNumber: noSurat,
        contentType: 1,
        subject: perihal,
        summary: ringkasan,
        senderAddress: alamat,
        idUnit,
        idContentSpeed: idSpeed,
        idContentTopic: idTopic,
        idContentTrait: trait,
        idExternalContact: newExtSave ? '' : idExt,
        textExternalContact: textExt,
        isPrivate: +isPrivate,
        isSaveExtContact: newExtSave ? 1 : 0,
        isWithoutAttachment: 1, // sek
        // attachments array -> name, content, size
        recipients: arrInt,
        carbonCopies: arrCc,
      })
      .then(resp => {
        const {code, error_message} = resp.data.system;

        if (code === 215) {
          alert('Berhasil Tambah Surat');
          goBack();
        } else {
          console.log(code);
          console.log(error_message);
          alert(error_message);
        }
      })
      .catch(error => {
        alert('Gagal mengolah data');
        console.log(error);
      });
  };

  // Handle Pilih Tanggal Surat
  const handleDate = (event, pickedDate) => {
    toggleShowDate(false);

    if (pickedDate) {
      let dd = String(pickedDate.getDate()).padStart(2, '0');
      let mm = String(pickedDate.getMonth() + 1).padStart(2, '0'); //January = 0
      let yyyy = pickedDate.getFullYear();

      let fullDate = dd + '-' + mm + '-' + yyyy;

      setContentDate(pickedDate);
      setStringDate(fullDate);
    }
  };

  // Handle Pilih yang hanya satu pilihan
  const handleSingleChoice = (type, id, text) => {
    switch (type) {
      case 'speed':
        setIdSpeed(id);
        setTextSpeed(text);
        toggleModal({...isModalOpen, speed: false});
        break;

      case 'ext':
        setIdExt(id);
        setTextExt(text);
        toggleModal({...isModalOpen, ext: false});
        break;

      case 'topic':
        setIdTopic(id);
        setTextTopic(text);
        toggleModal({...isModalOpen, topic: false});
        break;

      case 'unit':
        setIdUnit(id);
        setTextUnit(text);
        toggleModal({...isModalOpen, unit: false});
        break;

      default:
        alert('salah');
        break;
    }
  };

  // Handle Pilih yang bisa lebih dari satu pilihan
  const handleMultiChoice = (type, text) => {
    let arrayToPost = [];
    let updatedDataForState = [];
    switch (type) {
      case 'int':
        // Update data terpilih jadi punya value sebaliknya -> true/false
        let objId = fetchedInt.findIndex(obj => obj.rec_int_text === text);
        let objBaru = fetchedInt[objId];
        objBaru = {...objBaru, isChecked: !objBaru.isChecked};
        let newData = Object.assign([...fetchedInt], {[objId]: objBaru});

        // Pass data yang isChecked === true
        for (let i = 0; i < newData.length; i++) {
          const item = newData[i];
          if (item.isChecked) {
            let valueBaru = {
              iduser: item.iduser,
              idposition: item.idposition,
              idunit: item.idunit,
              textint_recipient: item.rec_int_text,
            };
            arrayToPost = [...arrayToPost, valueBaru];
          }
        }

        updatedDataForState = newData;

        setFetchedInt(updatedDataForState);
        setArrayInt(arrayToPost);
        break;

      case 'cc':
        // Update data terpilih jadi punya value sebaliknya -> true/false
        let objId2 = fetchedCc.findIndex(obj2 => obj2.rec_int_text === text);
        let objBaru2 = fetchedCc[objId2];
        objBaru2 = {...objBaru2, isChecked: !objBaru2.isChecked};
        let newData2 = Object.assign([...fetchedCc], {[objId2]: objBaru2});

        // Pass data yang isChecked === true
        for (let i = 0; i < newData2.length; i++) {
          const item = newData2[i];
          if (item.isChecked) {
            let valueBaru = {
              iduser: item.iduser,
              idposition: item.idposition,
              idunit: item.idunit,
              textint_recipient: item.rec_int_text,
            };
            arrayToPost = [...arrayToPost, valueBaru];
          }
        }

        updatedDataForState = newData2;

        setFetchedCc(updatedDataForState);
        setArrayCc(arrayToPost);
        break;

      default:
        alert('salah');
        break;
    }
  };

  return (
    <ScrollView contentContainerStyle={styles.containerUtama}>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />

      {/* Toggle isPrivate  */}
      <Text style={styles.textKeteranganAtas}>
        Pilih jika surat yang hanya bisa dilihat penerima
      </Text>
      <TouchableOpacity
        style={{flexDirection: 'row', alignItems: 'center', marginVertical: 5}}
        onPress={() => togglePrivate(!isPrivate)}>
        <CheckBox
          value={isPrivate}
          onValueChange={() => togglePrivate(!isPrivate)}
        />
        <Text>Private</Text>
      </TouchableOpacity>

      {/* Pilih Trait  */}
      <FormTraitSurat setTrait={setTrait} />

      {/* Input Nomor Surat  */}
      <RegularForm
        placeholder="Masukkan Nomor Surat"
        value={noSurat}
        onChange={setNoSurat}
      />

      {/* Pilih Tanggal Surat  */}
      <DatePicker
        isShowDate={isShowDate}
        value={contentDate}
        onChange={handleDate}
        stringDate={stringDate}
        onPress={() => toggleShowDate(true)}
      />

      {/* Pilih Speed/Prioritas Surat  */}
      <PickListData
        onPress={() => toggleModal({...isModalOpen, speed: true})}
        text="Pilih Prioritas Surat"
      />
      {idSpeed.length > 0 && (
        <View style={styles.itemTerpilih}>
          <Text style={{fontSize: 15}}>{textSpeed}</Text>
        </View>
      )}

      {/* Pilih Pengirim/External Contact  */}
      <PickListData
        onPress={() => toggleModal({...isModalOpen, ext: true})}
        text="Pilih Pengirim"
      />
      {idExt.length > 0 && (
        <View style={styles.itemTerpilih}>
          <Text style={{fontSize: 15}}>
            {newExtSave === '' ? textExt : newExtSave}
          </Text>
        </View>
      )}

      {/* Input Alamat Pengirim  */}
      <RegularForm placeholder="Alamat" value={alamat} onChange={setAlamat} />

      {/* Pilih Penerima/Internal Contact  */}
      <PickListData
        onPress={() => toggleModal({...isModalOpen, int: true})}
        text="Pilih Penerima"
      />
      {fetchedInt.map((item, key) => {
        if (item.isChecked) {
          return (
            <View key={key} style={styles.itemTerpilih}>
              <Text style={{fontSize: 15}}>
                {item.positionname} - {item.nameunit}
              </Text>
            </View>
          );
        }
      })}

      {/* Pilih CC/Tembusan/Internal Contact  */}
      <PickListData
        onPress={() => toggleModal({...isModalOpen, cc: true})}
        text="Pilih Tembusan"
      />
      {fetchedCc.map((item, key) => {
        if (item.isChecked) {
          return (
            <View key={key} style={styles.itemTerpilih}>
              <Text style={{fontSize: 15}}>
                {item.positionname} - {item.nameunit}
              </Text>
            </View>
          );
        }
      })}

      {/* Pilih Unit  */}
      <PickListData
        onPress={() => toggleModal({...isModalOpen, unit: true})}
        text="Pilih Unit Surat"
      />
      {idUnit.length > 0 && (
        <View style={styles.itemTerpilih}>
          <Text style={{fontSize: 15}}>{textUnit}</Text>
        </View>
      )}

      {/* Pilih Topic  */}
      <PickListData
        onPress={() => toggleModal({...isModalOpen, topic: true})}
        text="Pilih Topik Surat"
      />
      {idTopic.length > 0 && (
        <View style={styles.itemTerpilih}>
          <Text style={{fontSize: 15}}>{textTopic}</Text>
        </View>
      )}

      {/* Input Perihal Surat  */}
      <RegularForm
        placeholder="Perihal"
        value={perihal}
        onChange={setPerihal}
      />

      {/* Input Ringkasan Surat  */}
      <RegularForm
        placeholder="Ringkasan"
        value={ringkasan}
        onChange={setRingkasan}
      />

      {/* Button addContent */}
      <TouchableOpacity onPress={addContent}>
        <LinearGradient
          style={styles.containerDuaTombolPalingBawah}
          start={{x: 0, y: 0}}
          end={{x: 1, y: 0}}
          colors={['#38ee7d', '#11988d']}>
          <Text style={styles.textTerapkan}>Tambah Surat</Text>
        </LinearGradient>
      </TouchableOpacity>

      <View style={{marginTop: 100}} />

      {/* Modals  */}
      {/* Modal Speed  */}
      <ModalSpeed
        isModalOpen={isModalOpen.speed}
        closeModal={() => toggleModal({...isModalOpen, speed: false})}
        data={fetchedSpeed}
        onChoose={handleSingleChoice}
      />

      {/* Modal Pengirim  */}
      <ModalPengirim
        isModalOpen={isModalOpen.ext}
        closeModal={() => toggleModal({...isModalOpen, ext: false})}
        data={fetchedExt}
        onChangeText={setExtKeyword}
        onWriteNewExt={setNewExtSave}
        onChoose={handleSingleChoice}
        onClickSave={() =>
          newExtSave !== '' && toggleModal({...isModalOpen, ext: false})
        }
      />

      {/* Modal Penerima  */}
      <ModalPenerima
        isModalOpen={isModalOpen.int}
        closeModal={() => toggleModal({...isModalOpen, int: false})}
        data={fetchedInt}
        onChangeText={setIntKeyword}
        onChoose={handleMultiChoice}
        type="int"
      />

      {/* Modal CC  */}
      <ModalCc
        isModalOpen={isModalOpen.cc}
        closeModal={() => toggleModal({...isModalOpen, cc: false})}
        data={fetchedCc}
        onChangeText={setIntKeyword}
        onChoose={handleMultiChoice}
        type="cc"
      />

      {/* Modal Unit  */}
      <ModalUnit
        isModalOpen={isModalOpen.unit}
        closeModal={() => toggleModal({...isModalOpen, unit: false})}
        data={fetchedUnit}
        onChoose={handleSingleChoice}
      />

      {/* Modal Topic  */}
      <ModalTopik
        isModalOpen={isModalOpen.topic}
        closeModal={() => toggleModal({...isModalOpen, topic: false})}
        data={fetchedTopic}
        onChangeText={setTopicKeyword}
        onChoose={handleSingleChoice}
      />
    </ScrollView>
  );
};

AddSuratMasuk.navigationOptions = ({navigation: {goBack, navigate}}) => ({
  headerTitle: 'Buat Surat Masuk',
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="back"
        color="#424242"
        iconName="arrow-left"
        onPress={() => goBack()}
      />
    </NavHeaderButtons>
  ),
});

const styles = StyleSheet.create({
  // baru
  itemTerpilih: {
    padding: 8,
    marginTop: 7,
    backgroundColor: '#d3d3d3',
    borderRadius: 3,
  },
  //
  containerUtama: {
    marginTop: 5,
    marginBottom: 15,
    paddingHorizontal: 15,
    paddingBottom: 100,
  },
  containerPilih: {
    flexDirection: 'row',
    marginTop: 10,
    borderWidth: 1,
    borderColor: '#eeeeee',
    borderRadius: 3,
    paddingHorizontal: 17,
    paddingVertical: 11,
  },
  containerTextInput: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: 'black',
    borderBottomWidth: 0.75,
  },
  containerModalTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    paddingHorizontal: 15,
  },
  containerDuaTombolPalingBawah: {
    marginTop: 15,
    paddingTop: 7,
    paddingBottom: 10,
    borderRadius: 2,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  textInput: {
    flex: 1,
    marginLeft: 10,
    fontSize: 18,
  },
  textKeteranganAtas: {
    fontSize: 14,
    fontFamily: 'OpenSans-Regular',
    color: '#424242',
    marginTop: 8,
  },
  textPilih: {
    fontSize: 15,
    fontFamily: 'OpenSans-SemiBold',
    color: '#424242',
    marginLeft: 17,
  },
  textTerapkan: {
    fontSize: 18,
    color: '#FFFFFF',
    fontFamily: 'SourceSansPro-Semibold',
  },
  modalSingleChoose: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    backgroundColor: '#fff',
    padding: 12,
  },
});

export default AddSuratMasuk;
