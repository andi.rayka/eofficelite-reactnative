import React, {useState, useEffect} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  StyleSheet,
  Image,
  Modal,
  Dimensions,
} from 'react-native';

import {apiMaster2Param} from 'rootapp/src/services/api';
import Icon from 'react-native-vector-icons/Fontisto';

const FormTraitSurat = ({setTrait}) => {
  const [isModalOpen, toggleModalOpen] = useState(false);
  const [fetchedData, setFetchedData] = useState([]);
  const [textTrait, setTextTrait] = useState('Rutin');

  const ModalItem = ({imgSrc, text, onPress}) => (
    <TouchableOpacity
      onPress={onPress}
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 5,
      }}>
      <Image
        source={{uri: `mail_type_${imgSrc}`}}
        style={styles.imageEnvelopeTraitSurat}
      />
      <Text style={styles.textOptionTraitSurat}>{text}</Text>
    </TouchableOpacity>
  );

  useEffect(() => {
    apiMaster2Param('/getContentTrait', 403, setFetchedData);
  }, []);

  const handleChoose = (id, name) => {
    toggleModalOpen(false);
    setTextTrait(name);
    setTrait(id);
  };

  return (
    <>
      <Modal
        animationType="fade"
        transparent
        visible={isModalOpen}
        onRequestClose={() => toggleModalOpen(false)}>
        <TouchableOpacity
          onPress={() => toggleModalOpen(false)}
          style={styles.containerFreespace}
        />

        <View style={styles.containerModalContent}>
          {fetchedData.map((item, key) => {
            return (
              <ModalItem
                key={key}
                imgSrc={item.contenttrait_name.toLowerCase()}
                text={item.contenttrait_name}
                onPress={() =>
                  handleChoose(item.idcontenttrait, item.contenttrait_name)
                }
              />
            );
          })}
        </View>
      </Modal>

      <TouchableOpacity
        style={styles.containerOptionTraitSurat}
        onPress={() => toggleModalOpen(true)}>
        <View flexDirection="row" alignItems="center">
          <Image
            source={{uri: `mail_type_${textTrait.toLowerCase()}`}}
            style={styles.imageEnvelopeTraitSurat}
          />
          <Text style={styles.textOptionTraitSurat}>Surat {textTrait}</Text>
        </View>
        <Icon name="angle-down" style={styles.iconArrowDown} />
      </TouchableOpacity>
    </>
  );
};

const styles = StyleSheet.create({
  containerOptionTraitSurat: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderWidth: 1,
    borderColor: '#38ee7d',
    borderRadius: 3,
    marginTop: 5,
    paddingHorizontal: 14,
    paddingTop: 6,
    paddingBottom: 7,
  },
  containerFreespace: {
    flex: 1,
    backgroundColor: '#000000',
    opacity: 0.2,
  },
  containerModalContent: {
    width: Dimensions.get('window').width - 30,
    position: 'absolute',
    top: Dimensions.get('window').width * 0.45,
    borderRadius: 2,
    alignSelf: 'center',
    backgroundColor: '#FFFFFF',
    padding: 14,
    paddingTop: 10,
  },
  imageEnvelopeTraitSurat: {
    width: 36,
    height: 38,
  },
  iconArrowDown: {
    fontSize: 20,
    color: '#38ee7d',
  },
  textOptionTraitSurat: {
    fontSize: 16,
    fontFamily: 'OpenSans-SemiBold',
    color: '#424242',
    marginLeft: 15,
  },
});

export default FormTraitSurat;
