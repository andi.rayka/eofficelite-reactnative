import AsyncStorage from '@react-native-community/async-storage';
import {apiMaster} from 'rootapp/src/services/api';
import {addCheckedValueToObj} from 'rootapp/src/commonFunctions';

// Get Internal Contact
export const getInternalContact = async (keyword, funcInt, funcCc) => {
  const token = await AsyncStorage.getItem('mainLoginToken');

  await apiMaster
    .post('/getReceiverIntList', {
      token,
      keyword,
    })
    .then(resp => {
      const {code, error_message} = resp.data.system;
      const {data} = resp.data;

      const finalValue = addCheckedValueToObj(data);

      if (code === 405) {
        funcInt(finalValue);
        funcCc(finalValue);
      } else {
        alert(error_message);
      }
    })
    .catch(error => {
      alert('Gagal mengambil data');
      console.log(error);
    });
};

// Get Topic or External Contact
export const getTopicOrExt = async (link, keyword, successCode, setData) => {
  const token = await AsyncStorage.getItem('mainLoginToken');

  await apiMaster
    .post(link, {
      token,
      keyword,
    })
    .then(resp => {
      const {code, error_message} = resp.data.system;
      const {data} = resp.data;

      if (code === successCode) {
        setData(data);
      } else {
        alert(error_message);
      }
    })
    .catch(error => {
      alert('Gagal mengambil data');
      console.log(error);
    });
};
