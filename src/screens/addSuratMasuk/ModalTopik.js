import React from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  Modal,
  Dimensions,
  FlatList,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient/';
import IcFontisto from 'react-native-vector-icons/Fontisto';

const ModalTopik = ({
  isModalOpen,
  closeModal,
  data,
  onChangeText,
  onChoose,
}) => {
  const renderItem = ({item}) => {
    return (
      <TouchableOpacity
        style={{
          backgroundColor: '#fff',
          borderRadius: 3,
          padding: 15,
          marginTop: 10,
          flexDirection: 'row',
          alignItems: 'center',
        }}
        onPress={() => onChoose('topic', item.idcontenttopic, item.topictext)}>
        {/* <View
          style={{
            borderRadius: 50,
            backgroundColor: 'purple',
            width: 35,
            height: 35,
          }}
        /> */}
        <View style={{paddingHorizontal: 5, marginLeft: 5}}>
          <Text
            style={{
              fontSize: 14,
              fontFamily: 'SourceSansPro-Regular',
              color: '#424242',
            }}>
            {item.topictext}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <Modal
      animationType="fade"
      transparent
      visible={isModalOpen}
      onRequestClose={closeModal}>
      <View style={styles.containerMain}>
        {/* Modal Head */}
        <View style={styles.containerHead}>
          <View style={styles.containerTitle}>
            <Text style={styles.textTitle}>List Topik</Text>
            <TouchableOpacity onPress={closeModal}>
              <IcFontisto name="close-a" size={20} color="#424242" />
            </TouchableOpacity>
          </View>

          {/* Input Keyword */}
          <View style={styles.containerInput}>
            <TextInput
              placeholder="Cari Topik"
              placeholderTextColor="#424242"
              style={styles.textInput}
              onChangeText={onChangeText}
            />
            <TouchableOpacity>
              <LinearGradient
                colors={['#38ee7d', '#11988d']}
                style={{paddingVertical: 12, paddingHorizontal: 13}}>
                <IcFontisto name="search" size={20} color="#fff" />
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.containerBody}>
          <FlatList
            data={data}
            renderItem={renderItem}
            keyExtractor={(item, key) => key.toString()}
          />
          <View style={{marginTop: 30}} />
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  containerMain: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    backgroundColor: 'transparent',
  },
  containerHead: {
    backgroundColor: '#fff',
    paddingHorizontal: 16,
    paddingTop: 20,
    paddingBottom: 20,
    elevation: 3,
  },
  containerTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 22,
  },
  containerInput: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 3,
    borderWidth: 1,
    borderColor: '#38ee7d',
    marginTop: 13,
  },
  containerBody: {
    flex: 1,
    backgroundColor: '#f5f5f5',
    paddingTop: 5,
    paddingHorizontal: 16,
  },
  textTitle: {
    fontSize: 20,
    fontFamily: 'OpenSans-SemiBold',
    color: '#424242',
  },
  textInput: {
    flex: 1,
    fontSize: 15,
    fontFamily: 'OpenSans-SemiBold',
    color: '#424242',
    marginLeft: 16,
    paddingVertical: 8,
  },
});

export default ModalTopik;
