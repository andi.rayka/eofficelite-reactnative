import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  Button,
  Alert,
  PermissionsAndroid,
  Image,
} from 'react-native';

import DocumentPicker from 'react-native-document-picker';
import RNFS from 'react-native-fs';
import RNFetchBlob from 'rn-fetch-blob';
import {apiAttachment} from 'rootapp/src/services/api';
import AsyncStorage from '@react-native-community/async-storage';

const CobaDownload = () => {
  // useEffect(() => {
  //   const request_storage_runtime_permission = async () => {
  //     try {
  //       const granted = await PermissionsAndroid.request(
  //         PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
  //         {
  //           title: 'ReactNativeCode Storage Permission',
  //           message:
  //             'ReactNativeCode App needs access to your storage to download Photos.',
  //           buttonNeutral: 'Tanya lagi nanti',
  //           buttonNegative: 'Tolak',
  //           buttonPositive: 'Izinkan',
  //         },
  //       );
  //       if (granted === PermissionsAndroid.RESULTS.GRANTED) {
  //         Alert.alert('Storage Permission Granted.');
  //       } else {
  //         Alert.alert('Storage Permission Not Granted');
  //       }
  //     } catch (err) {
  //       console.warn(err);
  //     }
  //   };

  //   request_storage_runtime_permission();
  // }, []);

  // const decode = () => {
  //   let dirs = RNFetchBlob.fs.dirs;
  //   let path = dirs.DCIMDir + '/coba.txt';
  //   RNFetchBlob.fs.writeFile(path, 'VGV4dDI=', 'base64').then(res => {
  //     console.log('File : ', res);
  //     alert('berhasil: ' + res);
  //   });
  // };

  const downloadFile = async idAttachment => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
        // {
        //   title: 'Izin menyimpan file',
        //   message:
        //     'ReactNativeCode App needs access to your storage to download Photos.',
        //   buttonNeutral: 'Tanya lagi nanti',
        //   buttonNegative: 'Tolak',
        //   buttonPositive: 'Izinkan',
        // },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        const token = await AsyncStorage.getItem('mainLoginToken');

        await apiAttachment
          .post('/getAttachment', {
            token,
            idAttachment,
          })
          .then(resp => {
            const {code, error_message} = resp.data.system;
            const {filename, filecontent} = resp.data.data;

            if (code === 234) {
              console.log('filename: ', filename);

              let dirs = RNFetchBlob.fs.dirs;
              let path = `${dirs.DownloadDir}/${filename}`;
              RNFetchBlob.fs
                .writeFile(path, filecontent, 'base64')
                .then(res => {
                  console.log('File terdownload: ', res);
                  alert('File berhasil diunduh di folder download');
                })
                .catch(error => {
                  alert('Gagal menyimpan data ke penyimpanan');
                  console.log(error);
                });
            } else {
              alert(error_message);
            }
          })
          .catch(error => {
            alert('Gagal download data dari server');
            console.log(error);
          });
      } else {
        Alert.alert('Tidak mendapat izin sehingga tidak bisa menyimpan file');
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <View style={{padding: 25, paddingTop: 50}}>
      <Button
        title="Download File Attacahment"
        onPress={() => downloadFile('44c08852-f163-41ae-a073-b34f4384239a')}
      />
    </View>
  );
};

export default CobaDownload;
