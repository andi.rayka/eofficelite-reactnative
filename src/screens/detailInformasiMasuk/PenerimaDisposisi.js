import React, {useState} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

const PenerimaDisposisi = ({textext_act, unit_name, recipients}) => {
  const [openMore, toggleOpenMore] = useState(false);

  const {
    textDariKepada,
    textNama,
    textUnit,
    textTampilkanLebih,
    textLebihBanyak,
    buttonLebihBanyak,
  } = styles;

  return (
    <View style={{marginTop: 18, paddingLeft: 15, paddingRight: 16}}>
      <View style={{flexDirection: 'row'}}>
        <Text style={textDariKepada}>Dari</Text>
        <View style={{flex: 2.75}}>
          <Text style={textNama}>{textext_act}</Text>
          <Text style={textUnit}>{unit_name}</Text>
        </View>
      </View>

      <View style={{flexDirection: 'row'}}>
        <Text style={textDariKepada}>Kepada</Text>
        <View style={{flex: 2.75}}>
          {recipients.map((item, key) => {
            return (
              <View key={key}>
                <Text style={textNama}>{item.textint_recipient}</Text>
                <Text style={textUnit}>
                  {item.carecstatus_name ? item.carecstatus_name : '-'}
                </Text>
              </View>
            );
          })}

          {/* {recipients.length > 2 && <TouchableOpacity onPress={() => toggleOpenMore(!openMore)}>
            <Text style={textTampilkanLebih}>
              {openMore ? 'Tutup' : '+ Tampilkan 4 penerima lainnya'}
            </Text>
          </TouchableOpacity>} */}
        </View>
      </View>

      {/* <TouchableOpacity style={buttonLebihBanyak}>
        <Text style={textLebihBanyak}>Detail</Text>
      </TouchableOpacity> */}
    </View>
  );
};

const styles = StyleSheet.create({
  textDariKepada: {
    fontSize: 14,
    color: '#424242',
    fontFamily: 'OpenSans-SemiBold',
    flex: 1,
  },
  textNama: {
    fontSize: 15,
    color: '#212121',
    fontFamily: 'SourceSansPro-Regular',
  },
  textUnit: {
    fontSize: 14,
    color: '#9e9e9e',
    fontFamily: 'SourceSansPro-Regular',
    marginBottom: 5,
  },
  textTampilkanLebih: {
    fontSize: 15,
    color: '#37d57a',
    fontFamily: 'SourceSansPro-SemiBold',
  },
  textLebihBanyak: {
    fontSize: 14,
    color: '#424242',
    fontFamily: 'OpenSans-SemiBold',
  },
  buttonLebihBanyak: {
    backgroundColor: '#ffffff',
    borderWidth: 1,
    borderRadius: 3,
    borderColor: '#37d67a',
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 7,
    paddingBottom: 6,
    marginTop: 20,
  },
});

export default PenerimaDisposisi;
