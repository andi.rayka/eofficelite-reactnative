import React, {useState} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

import {downloadLampiran} from 'rootapp/src/commonFunctions';

import Icon from 'react-native-vector-icons/Fontisto';

const LampiranInformasi = ({attachments}) => {
  const [openLampiran, toggleOpenLampiran] = useState(false);

  const {textTampilkanLampiran, containerButtonToggle} = styles;

  return (
    <View style={containerButtonToggle}>
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}
        onPress={() => toggleOpenLampiran(!openLampiran)}>
        <Text style={textTampilkanLampiran}>
          {openLampiran ? 'Tutup' : 'Tampilkan Lampiran'}
        </Text>
        <Icon
          name={openLampiran ? 'angle-up' : 'angle-down'}
          style={{fontSize: 18, color: '#37d67a'}}
        />
      </TouchableOpacity>

      {openLampiran &&
        attachments &&
        attachments.map((item, key) => {
          return (
            <TouchableOpacity
              style={styles.containerLampiran}
              key={key}
              onPress={() => downloadLampiran(item.idattachment)}>
              <Icon name="paperclip" size={18} />
              <Text style={styles.textLampiran}>{item.filename}</Text>
            </TouchableOpacity>
          );
        })}
    </View>
  );
};

const styles = StyleSheet.create({
  containerButtonToggle: {
    marginTop: 15,
    paddingLeft: 15,
    paddingRight: 16,
    paddingVertical: 15,
    borderTopColor: 'rgba(0, 0, 0, 0.05)',
    borderTopWidth: 1,
  },
  containerLampiran: {
    flexDirection: 'row',
    borderWidth: 0.5,
    borderColor: '#38ee7d',
    borderRadius: 2,
    padding: 10,
    marginTop: 10,
  },
  textTampilkanLampiran: {
    fontSize: 14,
    color: '#424242',
    fontFamily: 'OpenSans-SemiBold',
    marginBottom: 10,
  },
  textLampiran: {
    fontSize: 16,
    color: '#424242',
    fontFamily: 'SourceSansPro-Regular',
    marginLeft: 5,
  },
});

export default LampiranInformasi;
