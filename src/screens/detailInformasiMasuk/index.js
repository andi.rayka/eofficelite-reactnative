import React, {useState, useEffect} from 'react';
import {View, Text, ScrollView, StyleSheet} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import {apiActivity} from 'rootapp/src/services/api';

import {
  NavHeaderButtons,
  Item,
} from 'rootapp/src/components/navigation/HeaderButton';
import LoadingScreen from 'rootapp/src/components/common/LoadingScreen';
import KopSurat from './KopSurat';
import KopDisposisi from './KopDisposisi';
import PenerimaSurat from './PenerimaSurat';
import PenerimaDisposisi from './PenerimaDisposisi';
import LampiranInformasi from './LampiranInformasi';
import Lampiran from './Lampiran';

const DetailInformasiMasuk = ({navigation: {navigate, getParam}}) => {
  const [fetchedInformation, setFetchedInformation] = useState();
  const [fetchedContent, setFetchedContent] = useState();
  const [isLoading, setLoading] = useState(true);

  // Initial Fetch
  useEffect(() => {
    const getDetailSurat = async () => {
      const idcontentactivity = getParam('idcontentactivity');
      const token = await AsyncStorage.getItem('mainLoginToken');

      await apiActivity
        .post('/getInformationDetail', {
          token,
          idInformation: idcontentactivity,
        })
        .then(resp => {
          const {code, error_message} = resp.data.system;
          const {information, content} = resp.data.data;

          if (code === 244) {
            setFetchedInformation(information);
            setFetchedContent(content);
            setLoading(false);
            console.log('Information: ', information);
            console.log('Content: ', content);
          } else {
            alert(error_message);
          }
        })
        .catch(error => {
          alert('Gagal mengambil data');
          console.log(error);
        });
    };

    getDetailSurat();
  }, [getParam]);

  return isLoading ? (
    <LoadingScreen />
  ) : (
    <ScrollView>
      <View style={styles.bgPutih}>
        <KopDisposisi
          contentnumber={fetchedInformation.contentactivitycode}
          subject={fetchedInformation.commands_text}
          summary={fetchedInformation.notes}
        />

        <PenerimaDisposisi
          textext_act={fetchedInformation.textint_act}
          // unit_name={unit_name}
          recipients={fetchedInformation.contentactivityrecipientlist}
        />

        <LampiranInformasi
          attachments={fetchedInformation.contentactivityattachmentlist}
        />
      </View>

      {/* Detail Surat  */}

      <Text
        style={{
          marginTop: 13,
          marginLeft: 10,
          fontSize: 22,
          color: '#212121',
          fontFamily: 'OpenSans-SemiBold',
        }}>
        Detail Surat Terkait
      </Text>
      <View style={styles.bgPutih}>
        <KopSurat
          contentnumber={fetchedContent.contentnumber}
          subject={fetchedContent.subject}
          summary={fetchedContent.summary}
        />

        <PenerimaSurat
          textext_act={fetchedContent.textext_act}
          unit_name={fetchedContent.unit_name}
          recipients={fetchedContent.recipients}
        />

        <Lampiran attachments={fetchedContent.attachments} />
      </View>
    </ScrollView>
  );
};

DetailInformasiMasuk.navigationOptions = ({
  navigation: {goBack, getParam},
}) => ({
  headerTitle: 'Detail Informasi',
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="back"
        color="#424242"
        iconName="arrow-left"
        onPress={() => goBack()}
      />
    </NavHeaderButtons>
  ),
});

const styles = StyleSheet.create({
  bgPutih: {
    backgroundColor: '#FFFFFF',
    paddingVertical: 10,
  },
  bgAbu: {
    backgroundColor: '#f5f5f5',
    paddingTop: 15,
    paddingBottom: 180,
    paddingLeft: 15,
    paddingRight: 17,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buttonKirim: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#37d67a',
    borderRadius: 3,
    paddingTop: 7,
    paddingBottom: 6,
    paddingLeft: 18,
    flex: 1,
    backgroundColor: '#ffffff',
  },
  textButton: {
    fontSize: 15,
    color: '#424242',
    fontFamily: 'OpenSans-SemiBold',
  },
});

export default DetailInformasiMasuk;
