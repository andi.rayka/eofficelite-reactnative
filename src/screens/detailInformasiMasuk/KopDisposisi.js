import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const KopDisposisi = ({contentnumber, subject, summary}) => {
  const {
    garisHorizontal,
    textStatusSurat,
    textInisialJenisSurat,
    textNomorSurat,
    textTgl,
    textJudulSurat,
    textIsiSurat,
  } = styles;

  const newcommands_text = subject.replace(/_##_/g, ', ');

  return (
    <View>
      <View style={{paddingLeft: 15, paddingRight: 16}}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={textInisialJenisSurat}>INFORMASI</Text>
            <Text style={textNomorSurat}>{contentnumber}</Text>
          </View>

          <Text style={textTgl}>05 Nov 2019</Text>
        </View>

        <Text style={textJudulSurat}>{newcommands_text}</Text>
        <Text style={textIsiSurat}>{summary}</Text>
      </View>

      <View style={{flexDirection: 'row', alignItems: 'center', marginTop: 15}}>
        <View style={[garisHorizontal, {flex: 1}]} />
        <Text style={textStatusSurat}>Belum dibaca</Text>
        <View style={[garisHorizontal, {width: 30}]} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  textInisialJenisSurat: {
    backgroundColor: '#2ccce4',
    borderRadius: 1,
    paddingHorizontal: 7,
    fontSize: 14,
    color: '#FFFFFF',
    fontFamily: 'OpenSans-Regular',
  },
  textNomorSurat: {
    marginLeft: 5,
    fontSize: 15,
    color: '#2ccce4',
    fontFamily: 'OpenSans-SemiBold',
  },
  textTgl: {
    fontSize: 14,
    color: '#424242',
    fontFamily: 'OpenSans-Regular',
  },
  textJudulSurat: {
    marginTop: 10,
    fontSize: 16,
    color: '#212121',
    fontFamily: 'OpenSans-SemiBold',
  },
  textIsiSurat: {
    marginTop: 5,
    fontSize: 15,
    color: '#424242',
    fontFamily: 'SourceSansPro-Regular',
  },
  textStatusSurat: {
    backgroundColor: '#37d67a',
    paddingVertical: 6,
    paddingHorizontal: 25,
    borderRadius: 20,
    fontSize: 14,
    color: '#ffffff',
    fontFamily: 'SourceSansPro-SemiBold',
  },
  garisHorizontal: {
    height: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.05)',
  },
});

export default KopDisposisi;
