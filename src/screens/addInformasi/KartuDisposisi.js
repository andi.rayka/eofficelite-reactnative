import React, {useState} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';

import Icon from 'react-native-vector-icons/Fontisto';
import Feather from 'react-native-vector-icons/Feather';

const KartuDiposisi = ({
  contentnumber,
  contentspeed_name,
  subject,
  summary,
}) => {
  const [openMore, toggleOpenMore] = useState(false);
  const [openKepada, toggleOpenKepada] = useState(false);
  const [openLampiran, toggleOpenLampiran] = useState(false);
  const datasementara = [1, 1, 11, 1, 1, 11, 1, 1, 11];

  return (
    <View style={styles.containerUtama}>
      <View style={{padding: 15}}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={styles.textNomorSurat}>{contentnumber}</Text>
          <Text style={styles.textDate}>17 Okt 2019</Text>
        </View>
        <Text style={styles.textJudulSurat}>{subject}</Text>
        <Text style={styles.textIsiSurat}>{summary}</Text>
      </View>

      <TouchableOpacity
        activeOpacity={0.6}
        onPress={() => toggleOpenMore(!openMore)}
        style={styles.containerSifatSurat}>
        <Text style={styles.textSifatSurat}>{contentspeed_name}</Text>
        {/* <Icon
          name={openMore ? 'angle-up' : 'angle-down'}
          style={{fontSize: 18, color: '#ffffff'}}
        /> */}
      </TouchableOpacity>

      {/* {openMore && (
        <View style={{paddingHorizontal: 15, paddingBottom: 15, paddingTop: 7}}>
          <View style={{flexDirection: 'row', marginTop: 8}}>
            <Text style={styles.textDariKepada}>Dari</Text>

            <View style={{flex: 3}}>
              <Text style={styles.textNama}>Prof. Dr. Muhadjir Effendy</Text>
            </View>
          </View>

          <View style={{flexDirection: 'row', marginTop: 8}}>
            <Text style={styles.textDariKepada}>Kepada</Text>
            <View style={{flex: 3}}>
              <Text style={styles.textNama}>"Didik Suhardi, Ph.D"</Text>
              {openKepada &&
                datasementara.map(
                  (item, key) =>
                    key > 1 && (
                      <Text key={key} style={styles.textNama}>
                        "Didik Suhardi, Ph.D"
                      </Text>
                    ),
                )}

              <TouchableOpacity onPress={() => toggleOpenKepada(!openKepada)}>
                <Text style={styles.textTampilkanLebih}>
                  {openKepada ? 'Tutup' : '+ 4 penerima lainnya'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>

          <View style={{flexDirection: 'row', marginTop: 8}}>
            <Text style={styles.textDariKepada}>Lampiran</Text>
            <View style={{flex: 3}}>
              <TouchableOpacity style={styles.containerLampiran}>
                <Feather name="paperclip" size={18} />
                <Text style={styles.textLampiran}>lampiran tadi.pdf</Text>
              </TouchableOpacity>
              {openLampiran &&
                datasementara.map(
                  (item, key) =>
                    key > 1 && (
                      <TouchableOpacity
                        style={styles.containerLampiran}
                        key={key}>
                        <Feather name="paperclip" size={18} />
                        <Text style={styles.textLampiran}>
                          lampiran tadi.pdf
                        </Text>
                      </TouchableOpacity>
                    ),
                )}

              <TouchableOpacity
                onPress={() => toggleOpenLampiran(!openLampiran)}>
                <Text style={styles.textTampilkanLebih}>
                  {openLampiran ? 'Tutup' : '+ 2 Lampiran'}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )}  */}
    </View>
  );
};

const styles = StyleSheet.create({
  containerUtama: {
    backgroundColor: '#ffffff',
    borderRadius: 5,
    marginHorizontal: 16,
    marginBottom: 20,
  },
  containerSifatSurat: {
    backgroundColor: '#2ccce4',
    paddingHorizontal: 15,
    paddingVertical: 6,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  containerLampiran: {
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 0.5,
    borderColor: '#38ee7d',
    padding: 3,
    marginBottom: 10,
  },
  textNomorSurat: {
    fontSize: 14,
    fontFamily: 'OpenSans-SemiBold',
    color: '#424242',
  },
  textDate: {
    fontSize: 14,
    fontFamily: 'OpenSans-Regular',
    color: '#424242',
  },
  textJudulSurat: {
    fontSize: 16,
    fontFamily: 'OpenSans-SemiBold',
    color: '#212121',
    marginTop: 15,
  },
  textIsiSurat: {
    fontSize: 15,
    fontFamily: 'SourceSansPro-Regular',
    color: '#424242',
    marginTop: 6,
  },
  textSifatSurat: {
    fontSize: 15,
    fontFamily: 'SourceSansPro-Regular',
    color: '#ffffff',
  },
  textDariKepada: {
    fontSize: 14,
    color: '#212121',
    fontFamily: 'OpenSans-Regular',
    flex: 1,
    marginBottom: 10,
  },
  textNama: {
    fontSize: 15,
    color: '#212121',
    fontFamily: 'SourceSansPro-SemiBold',
  },
  textTampilkanLebih: {
    fontSize: 15,
    color: '#37d57a',
    fontFamily: 'SourceSansPro-SemiBold',
    marginTop: 4,
  },
  textLampiran: {
    fontSize: 16,
    color: '#424242',
    fontFamily: 'SourceSansPro-Regular',
    marginLeft: 5,
  },
});

export default KartuDiposisi;
