import React, {useState, useEffect, useCallback} from 'react';
import {
  View,
  Text,
  Modal,
  StyleSheet,
  Dimensions,
  ImageBackground,
  ScrollView,
  StatusBar,
  TouchableOpacity,
  Button,
  CheckBox,
} from 'react-native';

import {apiActivity, apiMaster2Param} from 'rootapp/src/services/api';
import AsyncStorage from '@react-native-community/async-storage';
// import {getCommands} from './functions';
import {getCommands, getReceiverList} from './functions';
import DocumentPicker from 'react-native-document-picker';
import RNFS from 'react-native-fs';

import Icon from 'react-native-vector-icons/Fontisto';
import IconAwesome from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient/';
import {
  NavHeaderButtons,
  Item,
} from 'rootapp/src/components/navigation/HeaderButton';

import RegularForm from 'rootapp/src/components/form/SingleLine';
import PickListData from 'rootapp/src/components/form/PickListData';
import ModalPenerima from './ModalPenerima';
import KartuDisposisi from './KartuDisposisi';

const AddDisposisi = ({navigation: {getParam, navigate}}) => {
  const [fetchedCommands, setFetchedCommands] = useState([]);
  const [notes, setNotes] = useState('');

  // Receiver
  const [receiverKeyword, setReceiverKeyword] = useState('');
  const [fetchedReceiver, setFetchedReceiver] = useState([]);

  // Modal
  const [isModalCommandsOpen, toggleModalCommands] = useState(false);
  const [isModalReceiverOpen, toggleModalReceiver] = useState(false);

  // Document diplih sebagai attachment
  const [pickedDocuments, setPickedDocuments] = useState([]);

  const contentDetail = getParam('contentDetail');

  // Get Commands
  useEffect(() => {
    getCommands(setFetchedCommands);
  }, []);

  // Get Receiver/Internal Contact List
  useEffect(() => {
    getReceiverList(receiverKeyword, setFetchedReceiver);
  }, [receiverKeyword]);

  //   Add Informasi
  const addInformasi = async () => {
    const token = await AsyncStorage.getItem('mainLoginToken');

    // buat array berisi idCommand yang dipilih
    let arrayidCommands = [];
    for (let i = 0; i < fetchedCommands.length; i++) {
      const item = fetchedCommands[i];
      if (item.isChecked) {
        arrayidCommands = [...arrayidCommands, item.idcommand];
      }
    }

    // buat array Receiver berisi iduser, idposition, idunit, rec_int_text yang dipilih
    let arrayReceiver = [];
    for (let i = 0; i < fetchedReceiver.length; i++) {
      const item = fetchedReceiver[i];
      if (item.isChecked) {
        let valueBaru = {
          iduser: item.iduser,
          idposition: item.idposition,
          idunit: item.idunit,
          textint_recipient: item.rec_int_text,
        };
        arrayReceiver = [...arrayReceiver, valueBaru];
      }
    }

    await apiActivity
      .post('/addInformationFromContent', {
        token,
        idCommands: arrayidCommands,
        idContent: contentDetail.idcontent,
        notes,
        recipients: arrayReceiver,
        attachments: pickedDocuments.length === 0 ? null : pickedDocuments,
      })
      .then(resp => {
        const {code, error_message} = resp.data.system;

        if (code === 241) {
          navigate('Home');
          alert('Berhasil Mengirim Informasi');
        } else {
          alert(error_message);
        }
      })
      .catch(error => {
        alert('Gagal mengolah data');
        console.log(error);
      });
  };

  const handleCommandChoice = id => {
    //   Mengupdate isChecked
    let objId = fetchedCommands.findIndex(obj => obj.idcommand === id);

    let valueBaru = fetchedCommands[objId];
    valueBaru = {...valueBaru, isChecked: !valueBaru.isChecked};

    let objBaru = Object.assign([...fetchedCommands], {
      [objId]: valueBaru,
    });

    setFetchedCommands(objBaru);
  };

  // Handle pilih penerima informasi
  const handleChooseReceiver = text => {
    // Update data terpilih jadi punya value sebaliknya -> true/false
    let objId = fetchedReceiver.findIndex(obj => obj.rec_int_text === text);

    let objBaru = fetchedReceiver[objId];
    objBaru = {...objBaru, isChecked: !objBaru.isChecked};

    let newData = Object.assign([...fetchedReceiver], {[objId]: objBaru});

    setFetchedReceiver(newData);
  };

  const handleAttachment = async () => {
    try {
      const results = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });

      let pickedData = [];

      for (const res of results) {
        const {uri, name, size} = res;

        await RNFS.readFile(uri, 'base64')
          .then(resBase64 => {
            pickedData = [...pickedData, {name, content: resBase64, size}];
          })
          .catch(err => console.log(err));
      }

      console.log(pickedData);
      setPickedDocuments(pickedData);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  return (
    <ScrollView style={{flex: 1}}>
      <StatusBar backgroundColor="#34E27E" />

      {/* Header Halaman */}
      <ImageBackground
        source={{uri: 'bg_home'}}
        resizeMode="cover"
        style={{paddingBottom: 0}}>
        <KartuDisposisi
          contentnumber={contentDetail.contentnumber}
          contentspeed_name={contentDetail.contentspeed_name}
          subject={contentDetail.subject}
          summary={contentDetail.summary}
        />
      </ImageBackground>

      {/* Body halaman */}
      <View style={{paddingHorizontal: 15, paddingTop: 14}}>
        {/* Pilih Commands */}
        <PickListData
          onPress={() => toggleModalCommands(true)}
          text="Pilih Instruksi"
        />
        {fetchedCommands.map((item, key) => {
          if (item.isChecked) {
            return (
              <View
                key={key}
                style={{
                  padding: 8,
                  marginTop: 7,
                  backgroundColor: '#d3d3d3',
                  borderRadius: 3,
                }}>
                <Text style={{fontSize: 15}}>> {item.command_name}</Text>
              </View>
            );
          }
        })}

        {/* Pilih Penerima Informasi */}
        <PickListData
          onPress={() => toggleModalReceiver(true)}
          text="Pilih Penerima Informasi"
        />
        {fetchedReceiver.map((item, key) => {
          if (item.isChecked) {
            return (
              <View
                key={key}
                style={{
                  padding: 8,
                  marginTop: 7,
                  backgroundColor: '#d3d3d3',
                  borderRadius: 3,
                }}>
                <Text style={{fontSize: 15}}>
                  {item.positionname} - {item.nameunit}
                </Text>
              </View>
            );
          }
        })}

        {/* Pilih Attachment */}
        <PickListData onPress={handleAttachment} text="Pilih Lampiran" />
        {pickedDocuments.map((item, key) => {
          return (
            <View
              key={key}
              style={{
                padding: 8,
                marginTop: 7,
                backgroundColor: '#d3d3d3',
                borderRadius: 3,
              }}>
              <Text style={{fontSize: 15}}>
                {item.name}
                {/* - {item.size} kb */}
              </Text>
            </View>
          );
        })}

        {/* Input Catatan  */}
        <RegularForm
          placeholder="Catatan Lainnya"
          value={notes}
          onChange={setNotes}
        />

        <TouchableOpacity onPress={addInformasi}>
          <LinearGradient
            style={styles.containerDuaTombolPalingBawah}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={['#38ee7d', '#11988d']}>
            <Text style={styles.textTerapkan}>Kirim Informasi</Text>
          </LinearGradient>
        </TouchableOpacity>

        <View style={{marginTop: 50}} />
      </View>

      {/* Modals */}

      {/* Modal Penerima  */}
      <ModalPenerima
        isModalOpen={isModalReceiverOpen}
        closeModal={() => toggleModalReceiver(false)}
        data={fetchedReceiver}
        onChangeText={setReceiverKeyword}
        onChoose={handleChooseReceiver}
      />

      {/* Modal Commands */}
      <Modal
        animationType="fade"
        transparent
        visible={isModalCommandsOpen}
        onRequestClose={() => toggleModalCommands(false)}>
        <TouchableOpacity
          onPress={() => toggleModalCommands(false)}
          style={styles.containerFreespace}
        />

        <View style={styles.containerModalContent}>
          <View style={styles.containerJudulModal}>
            <Text style={styles.textJudulSection}>Pilih instruksi</Text>
            <TouchableOpacity onPress={() => toggleModalCommands(false)}>
              <IconAwesome name="times-circle" style={styles.iconClose} />
            </TouchableOpacity>
          </View>

          {fetchedCommands.map((item, key) => {
            return (
              <CheckItem
                key={key}
                value={item.isChecked}
                text={item.command_name}
                onChange={() => handleCommandChoice(item.idcommand)}
              />
            );
          })}

          <TouchableOpacity onPress={() => toggleModalCommands(false)}>
            <LinearGradient
              style={styles.containerDuaTombolPalingBawah}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              colors={['#38ee7d', '#11988d']}>
              <Text style={styles.textTerapkan}>Terapkan</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </Modal>

      <View style={{marginVertical: 20}} />
    </ScrollView>
  );
};

const CheckItem = ({value, text, onChange}) => {
  return (
    <TouchableOpacity
      onPress={onChange}
      style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
      <CheckBox value={value} onValueChange={onChange} />
      <Text>{text}</Text>
    </TouchableOpacity>
  );
};

AddDisposisi.navigationOptions = ({navigation: {getParam, goBack}}) => ({
  headerTitle: 'Kirim Informasi',
  headerStyle: {
    backgroundColor: '#34E27E',
    elevation: 0,
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 22,
    marginLeft: 0,
  },
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="back"
        color="#fff"
        iconName="arrow-left"
        onPress={() => goBack()}
      />
    </NavHeaderButtons>
  ),
  //   headerRight: (
  //     <NavHeaderButtons>
  //       <Item
  //         title="send"
  //         color="#fff"
  //         iconName="paper-plane"
  //         onPress={getParam('addDisposition')}
  //       />
  //     </NavHeaderButtons>
  //   ),
});

const styles = StyleSheet.create({
  containerFreespace: {
    flex: 1,
    backgroundColor: '#000000',
    opacity: 0.6,
  },
  containerModalContent: {
    width: Dimensions.get('window').width,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 19,
    paddingVertical: 15,
  },
  containerJudulModal: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  containerOptionFilter: {
    marginTop: 15,
  },
  containerDuaTombolPalingBawah: {
    marginTop: 15,
    paddingTop: 7,
    paddingBottom: 10,
    borderRadius: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconClose: {
    fontSize: 25,
  },
  inputPencarian: {
    borderWidth: 1,
    borderColor: '#eeeeee',
    borderRadius: 3,
    paddingHorizontal: 15,
    paddingVertical: 8,
    fontSize: 14,
    fontFamily: 'OpenSans',
    marginTop: 10,
  },
  textJudulSection: {
    fontSize: 16,
    color: '#424242',
    fontFamily: 'OpenSans-Semibold',
    marginVertical: 6,
  },
  textTerapkan: {
    fontSize: 18,
    color: '#FFFFFF',
    fontFamily: 'SourceSansPro-Semibold',
  },
});

export default AddDisposisi;
