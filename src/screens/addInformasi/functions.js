import AsyncStorage from '@react-native-community/async-storage';
import {apiMaster} from 'rootapp/src/services/api';
import {addCheckedValueToObj} from 'rootapp/src/commonFunctions';

export const getCommands = async functName => {
  const token = await AsyncStorage.getItem('mainLoginToken');

  //contentActivityType-> 1 = disposisi, 2 = informasi
  await apiMaster
    .post('/getUserCommands', {
      token,
      contentActivityType: 2,
    })
    .then(resp => {
      const {code, error_message} = resp.data.system;
      const {data} = resp.data;

      const finalValue = addCheckedValueToObj(data);

      if (code === 409) {
        functName(finalValue);
        console.log('commands informasi: ', finalValue);
      } else {
        alert(error_message);
      }
    })
    .catch(error => {
      alert('Gagal mengambil data instruksi');
      console.log(error);
    });
};

// Get ReceiverList
export const getReceiverList = async (keyword, funcName) => {
  const token = await AsyncStorage.getItem('mainLoginToken');

  await apiMaster
    .post('/getInformationReceiverIntList', {
      token,
      keyword,
    })
    .then(resp => {
      const {code, error_message} = resp.data.system;
      const {data} = resp.data;

      const finalValue = addCheckedValueToObj(data);

      if (code === 411) {
        funcName(finalValue);
        console.log('data: ', data);
      } else {
        alert(error_message);
      }
    })
    .catch(error => {
      alert('Gagal mengambil data penerima informasi');
      console.log(error);
    });
};
