import React from 'react';
import {View, Text, ImageBackground, StyleSheet, StatusBar} from 'react-native';

import {
  NavHeaderButtons,
  Item,
} from 'rootapp/src/components/navigation/HeaderButton';
import MainMenu from './Menu';
import Icon from 'react-native-vector-icons/FontAwesome5';

const Home = () => {
  return (
    <>
      <StatusBar backgroundColor="#34E27E" />
      <ImageBackground
        source={{uri: 'bg_home'}}
        resizeMode="cover"
        style={{flex: 1}}>
        <View style={styles.containerTitle}>
          <Icon name="city" size={75} color="#fff" />
          <Text style={styles.textTitle}>PT Integra Teknologi Solusi</Text>
        </View>
      </ImageBackground>

      <MainMenu />

      <View flex={1} />
    </>
  );
};

Home.navigationOptions = ({navigation: {navigate, openDrawer}}) => ({
  headerTitle: 'Persuratan',
  headerStyle: {
    backgroundColor: '#34E27E',
    elevation: 0,
  },
  headerTintColor: '#fff',
  headerTitleStyle: {
    fontFamily: 'OpenSans-SemiBold',
    fontSize: 22,
    marginLeft: 0,
  },
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="drawer"
        color="#fff"
        iconName="nav-icon"
        // onPress={() => alert('drawer')}
      />
    </NavHeaderButtons>
  ),
  headerRight: (
    <NavHeaderButtons>
      <Item
        title="notif"
        color="#fff"
        iconName="bell-alt"
        onPress={() => navigate('ListNotifikasi')}
      />
    </NavHeaderButtons>
  ),
});

const styles = StyleSheet.create({
  containerTitle: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 90,
  },
  textTitle: {
    fontSize: 22,
    fontFamily: 'OpenSans-SemiBold',
    color: '#FFFFFF',
    marginTop: 10,
  },
});

export default Home;
