import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StyleSheet,
  Dimensions,
} from 'react-native';

import {withNavigation} from 'react-navigation';
import Icon from 'react-native-vector-icons/FontAwesome5';

const MainMenu = ({navigation: {navigate}}) => {
  const TambahSurat = ({onPress, text}) => (
    <TouchableOpacity
      style={styles.buttonTambah}
      onPress={onPress}
      activeOpacity={0.6}>
      <Icon name="plus" style={styles.iconTambah} />
      <Text style={styles.textTambah}>{text}</Text>
    </TouchableOpacity>
  );

  const MenuBukaList = ({text, onPress, image}) => (
    <TouchableOpacity
      style={styles.buttonBukaList}
      onPress={onPress}
      activeOpacity={0.6}>
      <Image source={{uri: `ic_home_${image}`}} style={styles.imageBukaList} />
      <Text style={styles.textBukaList}>{text}</Text>
    </TouchableOpacity>
  );

  return (
    <View style={styles.containerUtama}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}>
        <TambahSurat
          text="Surat Masuk"
          onPress={() => navigate('AddSuratMasuk')}
        />
        <TambahSurat
          text="Surat Keluar"
          onPress={() => alert('buat surat keluar')}
        />
      </View>

      <View
        style={{
          flexDirection: 'row',
          flexWrap: 'wrap',
          justifyContent: 'space-between',
        }}>
        <MenuBukaList
          text="Surat Masuk"
          image="suratmasuk"
          onPress={() => navigate('ListSurat', {title: 'Surat Masuk'})}
        />
        <MenuBukaList
          text="Disposisi Masuk"
          image="disposisimasuk"
          onPress={() => navigate('ListDisposisi', {title: 'Disposisi Masuk'})}
        />
        <MenuBukaList
          text="Informasi Masuk"
          image="informasimasuk"
          onPress={() => navigate('ListInformasi', {title: 'Informasi Masuk'})}
        />
        <MenuBukaList
          text="Surat Keluar"
          image="suratkeluar"
          onPress={() => alert('Surat Keluar')}
          // onPress={() => navigate('ListSurat', {title: 'Surat Keluar'})}
        />
        <MenuBukaList
          text="Disposisi Keluar"
          image="disposisikeluar"
          onPress={() => alert('Disposisi Keluar')}
        />
        <MenuBukaList
          text="Informasi Keluar"
          image="informasikeluar"
          onPress={() => alert('Informasi Keluar')}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  containerUtama: {
    position: 'absolute',
    width: Dimensions.get('window').width * 0.9,
    height: Dimensions.get('window').height,
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 25,
  },
  buttonTambah: {
    flexDirection: 'row',
    borderRadius: 3,
    borderColor: '#ffffff',
    borderWidth: 1,
    paddingTop: 9,
    paddingBottom: 8,
    paddingHorizontal: 16,
    marginBottom: 5,
  },
  buttonBukaList: {
    width: '30%',
    backgroundColor: '#ffffff',
    marginVertical: 15,
    borderRadius: 3,
    elevation: 5,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingBottom: 10,
  },
  imageBukaList: {
    width: 60,
    height: 60,
  },
  iconTambah: {
    fontSize: 20,
    color: '#ffffff',
  },
  textTambah: {
    fontSize: 15,
    fontFamily: 'OpenSans-SemiBold',
    color: '#ffffff',
    marginLeft: 10,
  },
  textBukaList: {
    fontSize: 13,
    fontFamily: 'OpenSans-Regular',
    color: '#757575',
    textAlign: 'center',
  },
});

export default withNavigation(MainMenu);
