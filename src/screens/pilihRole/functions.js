import AsyncStorage from '@react-native-community/async-storage';
import {apiAuth} from 'rootapp/src/services/api';
import {setAsyncStorage} from 'rootapp/src/commonFunctions';

export const getAccess = async setFetchedData => {
  const token = await AsyncStorage.getItem('mainLoginToken');

  await apiAuth
    .post('/getAccess', {
      token,
    })
    .then(resp => {
      const {code, error_message} = resp.data.system;
      const {data} = resp.data;

      if (code === 116) {
        setFetchedData(data);
      } else {
        alert(error_message);
      }
    })
    .catch(error => {
      alert('Gagal mengambil data');
      console.log(error);
    });
};

export const setAccess = async (idPosition, idRole, idUnit, navigate) => {
  const token = await AsyncStorage.getItem('mainLoginToken');

  await apiAuth
    .post('/setAccess', {
      token,
      idPosition,
      idRole,
      idUnit,
    })
    .then(resp => {
      const {code, error_message} = resp.data.system;
      const {session} = resp.data.data;

      if (code === 117) {
        navigate('Home');
        console.log('Session: ', session);

        setAsyncStorage('mainUserData', JSON.stringify(session));
      } else {
        alert(error_message);
      }
    })
    .catch(error => {
      alert('Gagal set Role');
    });
};
