import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import {apiContent} from 'rootapp/src/services/api';

import {
  NavHeaderButtons,
  Item,
} from 'rootapp/src/components/navigation/HeaderButton';
import KopSurat from './KopSurat';
import PenerimaSurat from './PenerimaSurat';
import Lampiran from './Lampiran';
import ModalBatalSurat from './ModalBatalSurat';

const DetailSuratMasuk = ({
  navigation: {navigate, getParam, setParams, state},
}) => {
  const [fetchedData, setFetchedData] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const [isModalOpen, toggleOpenModal] = useState(false);

  //   const {params = {}} = state;
  //   const paramDesc = useRef(params.paramDesc);
  const setParam = useRef(setParams);

  const idContent = getParam('idcontent');

  useEffect(() => {
    const getDetailSurat = async () => {
      const token = await AsyncStorage.getItem('mainLoginToken');

      console.log('Token: ', token);

      await apiContent
        .post('/getContentDetail', {
          token,
          idContent,
        })
        .then(resp => {
          const {code, error_message} = resp.data.system;
          const {data} = resp.data;

          if (code === 227) {
            setFetchedData(data);

            if (data.iscancelled === 0) {
              setParam.current({batalSurat: () => toggleOpenModal(true)});
              setParam.current({notCancelled: true});
            }

            setLoading(false);
            console.log('detail surat : ', data);
          } else {
            alert(error_message);
          }
        })
        .catch(error => {
          alert('Gagal mengambil data');
          console.log(error);
        });
    };

    getDetailSurat();
  }, [idContent]);

  const {
    idcontent,
    contentnumber,
    subject,
    summary,
    textext_act,
    unit_name,
    recipients,
    attachments,
    iscancelled,
  } = fetchedData;

  return isLoading ? (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <ActivityIndicator size="large" color="#330066" animating />
    </View>
  ) : (
    <ScrollView>
      <ModalBatalSurat
        isModalOpen={isModalOpen}
        closeModal={() => toggleOpenModal(false)}
        idContent={idcontent}
      />

      <View style={styles.bgPutih}>
        <KopSurat
          contentnumber={contentnumber}
          subject={subject}
          summary={summary}
        />

        <PenerimaSurat
          textext_act={textext_act}
          unit_name={unit_name}
          recipients={recipients}
        />

        <Lampiran attachments={attachments} />

        {/* <Disposisi /> */}
      </View>

      <View style={styles.bgAbu}>
        {iscancelled !== '1' && (
          <>
            <TouchableOpacity
              style={[styles.buttonKirim, {marginRight: 10}]}
              onPress={() =>
                navigate('AddDisposisi', {contentDetail: fetchedData})
              }>
              <Text style={styles.textButton}>Kirim Disposisi</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.buttonKirim}
              onPress={() =>
                navigate('AddInformasi', {contentDetail: fetchedData})
              }>
              <Text style={styles.textButton}>Kirim Informasi</Text>
            </TouchableOpacity>
          </>
        )}
      </View>
    </ScrollView>
  );
};

DetailSuratMasuk.navigationOptions = ({navigation: {goBack, getParam}}) => ({
  headerTitle: 'Detail Surat',
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="back"
        color="#424242"
        iconName="arrow-left"
        onPress={() => goBack()}
      />
    </NavHeaderButtons>
  ),
  headerRight: getParam('notCancelled') && (
    <NavHeaderButtons>
      <Item
        title="cancel"
        color="#424242"
        iconName="undo"
        onPress={getParam('batalSurat')}
      />
      <Item
        title="confirm"
        color="#424242"
        iconName="check"
        onPress={() => alert('Konfirmasi')}
      />
    </NavHeaderButtons>
  ),
});

const styles = StyleSheet.create({
  bgPutih: {
    backgroundColor: '#FFFFFF',
    paddingVertical: 10,
  },
  bgAbu: {
    backgroundColor: '#f5f5f5',
    paddingTop: 15,
    paddingBottom: 180,
    paddingLeft: 15,
    paddingRight: 17,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buttonKirim: {
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#37d67a',
    borderRadius: 3,
    paddingTop: 7,
    paddingBottom: 6,
    paddingLeft: 18,
    flex: 1,
    backgroundColor: '#ffffff',
  },
  textButton: {
    fontSize: 15,
    color: '#424242',
    fontFamily: 'OpenSans-SemiBold',
  },
});

export default DetailSuratMasuk;
