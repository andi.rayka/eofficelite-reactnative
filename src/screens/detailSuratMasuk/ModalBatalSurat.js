import React, {useState} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  Modal,
  StyleSheet,
  Dimensions,
  TextInput,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import {apiContent} from 'rootapp/src/services/api';
import {withNavigation} from 'react-navigation';

import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient/';

const ModalBatalSurat = ({
  isModalOpen,
  closeModal,
  idContent,
  navigation: {navigate},
}) => {
  const [text, setText] = useState('');

  const batalkanSurat = async () => {
    const token = await AsyncStorage.getItem('mainLoginToken');

    await apiContent
      .post('/cancelContent', {
        token,
        idContent,
        cancelInfo: text,
      })
      .then(resp => {
        const {code, error_message} = resp.data.system;
        const {data} = resp.data;

        if (code === 232) {
          alert('Batalkan surat sukses');
          navigate('Home');
        } else {
          alert(error_message);
        }
      })
      .catch(error => {
        alert('Gagal mengambil data');
        console.log(error);
      });
  };

  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={isModalOpen}
      onRequestClose={closeModal}>
      <TouchableOpacity
        onPress={closeModal}
        style={styles.containerFreespace}
      />

      <View style={styles.containerModalContent}>
        <TouchableOpacity onPress={closeModal}>
          <Icon name="times-circle" style={{fontSize: 25}} />
        </TouchableOpacity>

        <Text style={{marginTop: 10}}>Alasan Membatalkan Surat:</Text>
        <TextInput
          value={text}
          onChangeText={setText}
          placeholder="Tulis di sini"
          style={{
            flex: 1,
            marginTop: 10,
            borderWidth: 1,
            borderColor: '#eeeeee',
            borderRadius: 3,
            fontSize: 15,
            fontFamily: 'OpenSans-SemiBold',
            color: '#424242',
            paddingHorizontal: 17,
            paddingVertical: 9,
          }}
        />
        <LinearGradient
          colors={['#38ee7d', '#11988d']}
          style={{marginTop: 10, borderRadius: 3}}>
          <TouchableOpacity style={{padding: 12}} onPress={batalkanSurat}>
            <Text style={{color: '#fff', fontSize: 16}}>Batalkan Surat</Text>
          </TouchableOpacity>
        </LinearGradient>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  containerFreespace: {
    flex: 1,
    backgroundColor: '#000000',
    opacity: 0.2,
  },
  containerModalContent: {
    width: Dimensions.get('window').width - 30,
    position: 'absolute',
    top: Dimensions.get('window').width * 0.45,
    borderRadius: 2,
    alignSelf: 'center',
    backgroundColor: '#FFFFFF',
    padding: 14,
    paddingTop: 10,
  },
});

export default withNavigation(ModalBatalSurat);
