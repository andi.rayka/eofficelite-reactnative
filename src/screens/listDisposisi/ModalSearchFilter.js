import React, {useState} from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  Modal,
  Dimensions,
  ScrollView,
  CheckBox,
} from 'react-native';

import LinearGradient from 'react-native-linear-gradient/';
import DatePicker1 from 'rootapp/src/components/form/DatePicker';
import DatePicker2 from 'rootapp/src/components/form/DatePicker';
import IcFontisto from 'react-native-vector-icons/Fontisto';

const ModalSearchFilter = ({isModalOpen, closeModal, onApply}) => {
  // SetKeyword
  const [keyword, setKeyword] = useState('');

  // Date
  const [date1, setDate1] = useState(new Date());
  const [stringDate1, setStringDate1] = useState('01-01-2019');
  const [isShowDate1, toggleShowDate1] = useState(false);
  const [date2, setDate2] = useState(new Date());
  const [stringDate2, setStringDate2] = useState('31-12-2019');
  const [isShowDate2, toggleShowDate2] = useState(false);

  // Dependent Value
  const [isContentNumber, toggleIsContentNumber] = useState(false);
  const [isSubject, toggleIsSubject] = useState(false);
  const [isContentActivityCode, toggleisContentActivityCode] = useState(false);
  const [isContentSender, toggleisContentSender] = useState(false);
  const [isSender, toggleisSender] = useState(false);
  const [isRecipient, toggleisRecipient] = useState(false);

  return (
    <Modal
      animationType="fade"
      transparent
      visible={isModalOpen}
      onRequestClose={closeModal}>
      <View style={styles.containerMain}>
        {/* Modal Head */}
        <View style={styles.containerHead}>
          <View style={styles.containerTitle}>
            <Text style={styles.textTitle}>Pencarian</Text>
            <TouchableOpacity onPress={closeModal}>
              <IcFontisto name="close-a" size={20} color="#424242" />
            </TouchableOpacity>
          </View>
        </View>

        {/* List Data Yang Dipilih User  */}
        <View style={{flex: 1, backgroundColor: '#fff'}}>
          <ScrollView contentContainerStyle={styles.containerBody}>
            {/* Input Keyword */}
            <View style={styles.containerInput}>
              <TextInput
                placeholder="Ketik Pencarian di Sini..."
                placeholderTextColor="#424242"
                style={styles.textInput}
                value={keyword}
                onChangeText={setKeyword}
              />
            </View>

            {/* Date Start Picker  */}
            <Text
              style={{
                fontSize: 15,
                fontFamily: 'OpenSans-Regular',
                color: '#424242',
                marginTop: 8,
              }}>
              Tanggal Awal
            </Text>
            <DatePicker1
              isShowDate={isShowDate1}
              value={date1}
              onChange={handledate1}
              stringDate={stringDate1}
              onPress={() => toggleShowDate1(true)}
            />

            {/* Date End Picker  */}
            <Text
              style={{
                fontSize: 15,
                fontFamily: 'OpenSans-Regular',
                color: '#424242',
                marginTop: 8,
              }}>
              Tanggal Akhir
            </Text>
            <DatePicker2
              isShowDate={isShowDate2}
              value={date2}
              onChange={handledate2}
              stringDate={stringDate2}
              onPress={() => toggleShowDate2(true)}
            />

            {/* Filter Search  */}
            <Text
              style={{
                fontSize: 15,
                fontFamily: 'OpenSans-Regular',
                color: '#424242',
                marginTop: 8,
              }}>
              Pencarian Jenis Lain
            </Text>
            {/* isContentNumber */}
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                borderColor: '#eeeeee',
                borderWidth: 1,
                paddingTop: 6,
                paddingBottom: 7,
                paddingHorizontal: 15,
                marginTop: 5,
              }}
              onPress={() => handleCheckFilterSearch('isContentNumber')}>
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: 'SourceSansPro-Semibold',
                  color: '#424242',
                }}>
                Nomor Surat
              </Text>
              <CheckBox
                value={isContentNumber}
                onChange={() => handleCheckFilterSearch('isContentNumber')}
              />
            </TouchableOpacity>

            {/* isSubject */}
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                borderColor: '#eeeeee',
                borderWidth: 1,
                paddingTop: 6,
                paddingBottom: 7,
                paddingHorizontal: 15,
                marginTop: 5,
              }}
              onPress={() => handleCheckFilterSearch('isSubject')}>
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: 'SourceSansPro-Semibold',
                  color: '#424242',
                }}>
                Perihal
              </Text>
              <CheckBox
                value={isSubject}
                onChange={() => handleCheckFilterSearch('isSubject')}
              />
            </TouchableOpacity>

            {/* isContentActivityCode */}
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                borderColor: '#eeeeee',
                borderWidth: 1,
                paddingTop: 6,
                paddingBottom: 7,
                paddingHorizontal: 15,
                marginTop: 5,
              }}
              onPress={() => handleCheckFilterSearch('isContentActivityCode')}>
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: 'SourceSansPro-Semibold',
                  color: '#424242',
                }}>
                Nomor Disposisi
              </Text>
              <CheckBox
                value={isContentActivityCode}
                onChange={() =>
                  handleCheckFilterSearch('isContentActivityCode')
                }
              />
            </TouchableOpacity>

            {/* isContentSender */}
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                borderColor: '#eeeeee',
                borderWidth: 1,
                paddingTop: 6,
                paddingBottom: 7,
                paddingHorizontal: 15,
                marginTop: 5,
              }}
              onPress={() => handleCheckFilterSearch('isContentSender')}>
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: 'SourceSansPro-Semibold',
                  color: '#424242',
                }}>
                Pengirim Surat
              </Text>
              <CheckBox
                value={isContentSender}
                onChange={() => handleCheckFilterSearch('isContentSender')}
              />
            </TouchableOpacity>

            {/* isSender */}
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                borderColor: '#eeeeee',
                borderWidth: 1,
                paddingTop: 6,
                paddingBottom: 7,
                paddingHorizontal: 15,
                marginTop: 5,
              }}
              onPress={() => handleCheckFilterSearch('isSender')}>
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: 'SourceSansPro-Semibold',
                  color: '#424242',
                }}>
                Pengirim Disposisi
              </Text>
              <CheckBox
                value={isSender}
                onChange={() => handleCheckFilterSearch('isSender')}
              />
            </TouchableOpacity>

            {/* isRecipient */}
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                borderColor: '#eeeeee',
                borderWidth: 1,
                paddingTop: 6,
                paddingBottom: 7,
                paddingHorizontal: 15,
                marginTop: 5,
              }}
              onPress={() => handleCheckFilterSearch('isRecipient')}>
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: 'SourceSansPro-Semibold',
                  color: '#424242',
                }}>
                Penerima Disposisi
              </Text>
              <CheckBox
                value={isRecipient}
                onChange={() => handleCheckFilterSearch('isRecipient')}
              />
            </TouchableOpacity>
          </ScrollView>
          <View
            style={{
              paddingBottom: 5,
              paddingHorizontal: 16,
            }}>
            {/* Button Reset CheckBox to false */}
            <TouchableOpacity
              onPress={resetSearch}
              style={{
                paddingTop: 7,
                paddingBottom: 10,
                borderRadius: 2,
                borderWidth: 1,
                borderColor: '#9e9e9e',
                width: '100%',
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom: 10,
              }}>
              <Text
                style={{
                  fontSize: 18,
                  color: '#616161',
                  fontFamily: 'SourceSansPro-Semibold',
                }}>
                Reset
              </Text>
            </TouchableOpacity>

            {/* Button Terapkan */}
            <TouchableOpacity onPress={closeModal}>
              <LinearGradient
                style={{
                  marginBottom: 27,
                  paddingTop: 7,
                  paddingBottom: 10,
                  borderRadius: 2,
                  width: '100%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}
                start={{x: 0, y: 0}}
                end={{x: 1, y: 0}}
                colors={['#38ee7d', '#11988d']}>
                <Text
                  style={{
                    fontSize: 18,
                    color: '#FFFFFF',
                    fontFamily: 'SourceSansPro-Semibold',
                  }}>
                  Terapkan
                </Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  containerMain: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,
    backgroundColor: 'transparent',
  },
  containerHead: {
    backgroundColor: '#fff',
    paddingHorizontal: 16,
    paddingTop: 20,
    paddingBottom: 15,
  },
  containerTitle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  containerInput: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 3,
    borderWidth: 1,
    borderColor: '#bdbdbd',
    marginTop: 13,
  },
  containerBody: {
    paddingTop: 5,
    paddingBottom: 100,
    paddingHorizontal: 16,
  },
  textTitle: {
    fontSize: 20,
    fontFamily: 'OpenSans-SemiBold',
    color: '#424242',
  },
  textInput: {
    flex: 1,
    fontSize: 15,
    fontFamily: 'OpenSans-Regular',
    color: '#424242',
    marginLeft: 16,
    paddingVertical: 8,
  },
});

export default ModalSearchFilter;
