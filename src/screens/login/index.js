import React, {useState} from 'react';
import {View, StatusBar, ImageBackground} from 'react-native';

import LinearGradient from 'react-native-linear-gradient';

import {apiAuth} from 'rootapp/src/services/api';
import {setAsyncStorage} from 'rootapp/src/commonFunctions';

import LoginForm from './Form';
import LoadingScreen from 'rootapp/src/components/common/LoadingScreen';

const Login = ({navigation: {navigate}}) => {
  const [isLoading, setLoading] = useState(false);

  const postLogin = async (username, password) => {
    setLoading(true);
    await apiAuth
      .post('/login', {
        username,
        password,
      })
      .then(resp => {
        const {code, token, error_message} = resp.data.system;

        if (code === 100) {
          // setAsyncStorage('currentRoute', 'PilihRole');
          setAsyncStorage('mainLoginToken', token);
          navigate('PilihRole');
        } else {
          alert(error_message);
          setLoading(false);
        }
      })
      .catch(error => {
        console.log(error);
        alert(error);
      });
  };

  return (
    <>
      <StatusBar translucent backgroundColor="transparent" />

      <ImageBackground source={{uri: 'bg_login'}} style={{flex: 1}}>
        <LinearGradient
          colors={['#38ee7d', '#11988d']}
          style={{flex: 1, opacity: 0.9, paddingHorizontal: 15}}>
          <View style={{flex: 1}} />

          {isLoading && <LoadingScreen />}
          <LoginForm
            onSubmit={(username, password) => postLogin(username, password)}
          />

          <View style={{marginBottom: 50}} />
        </LinearGradient>
      </ImageBackground>
    </>
  );
};

export default Login;
