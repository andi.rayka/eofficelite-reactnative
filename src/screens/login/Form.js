import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import Icon from 'react-native-vector-icons/Feather';

const LoginForm = ({onSubmit}) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [eyeOpen, eyeToggle] = useState(false);

  return (
    <View>
      <Text style={styles.title}>Login</Text>

      <View style={{marginTop: 45}} />

      <View>
        <Text style={styles.label}>Username</Text>
        <TextInput
          style={styles.form}
          underlineColorAndroid="#FFFFFF"
          autoCapitalize="none"
          returnKeyType="next"
          value={username}
          onChangeText={setUsername}
        />
      </View>

      <View style={{marginTop: 15}} />

      <View>
        <Text style={styles.label}>Password</Text>
        <TextInput
          style={styles.form}
          underlineColorAndroid="#FFFFFF"
          autoCapitalize="none"
          returnKeyType="next"
          value={password}
          onChangeText={setPassword}
          secureTextEntry={!eyeOpen}
        />

        <TouchableOpacity
          onPress={() => eyeToggle(!eyeOpen)}
          style={styles.iconContainer}>
          <Icon name={eyeOpen ? 'eye' : 'eye-off'} style={styles.icon} />
        </TouchableOpacity>
      </View>

      <View style={{marginTop: 30}} />

      <TouchableOpacity
        style={styles.buttonContainer}
        onPress={() => onSubmit(username, password)}>
        <Text style={styles.buttonText}>Log In</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    fontSize: 35,
    lineHeight: 47,
    color: '#FFFFFF',
    fontFamily: 'OpenSans-SemiBold',
  },
  label: {
    fontSize: 15,
    lineHeight: 18,
    color: '#FFFFFF',
    fontFamily: 'SourceSansPro-Regular',
  },
  form: {
    paddingTop: 3,
    fontSize: 17,
    lineHeight: 20,
    color: '#FFFFFF',
    fontFamily: 'SourceSansPro-Regular',
  },
  iconContainer: {
    position: 'absolute',
    right: 5,
    top: 15,
    padding: 5,
  },
  icon: {
    fontSize: 25,
    color: '#FFFFFF',
  },
  buttonContainer: {
    height: 50,
    backgroundColor: '#FFFFFF',
    borderRadius: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontFamily: 'SourceSansPro-SemiBold',
    color: '#616161',
    fontSize: 16,
    fontWeight: '600',
  },
});

export default LoginForm;
