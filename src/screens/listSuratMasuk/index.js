import React, {useState, useEffect, useRef} from 'react';
import {
  StatusBar,
  Button,
  Text,
  TouchableOpacity,
  Modal,
  StyleSheet,
  Dimensions,
  View,
  TextInput,
  ScrollView,
  CheckBox,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import {apiContent} from 'rootapp/src/services/api';

import {
  NavHeaderButtons,
  Item,
} from 'rootapp/src/components/navigation/HeaderButton';
import LoadingScreen from 'rootapp/src/components/common/LoadingScreen';
import ListSuratItem from './ListSuratItem';

import Icon from 'react-native-vector-icons/Fontisto';
import IconAwesome from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient/';
import DatePicker from '@react-native-community/datetimepicker';
import DatePicker2 from '@react-native-community/datetimepicker';

const CheckItem = ({value, text, onChange}) => {
  return (
    <TouchableOpacity
      onPress={onChange}
      style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
      <CheckBox value={value} onValueChange={onChange} />
      <Text>{text}</Text>
    </TouchableOpacity>
  );
};

const ListSurat = ({navigation: {setParams, state}}) => {
  const [fetchedData, setFetchedData] = useState([]);
  const [keyword, setKeyword] = useState('');

  // Date
  const [dateStart, setDateStart] = useState(new Date());
  const [stringDateStart, setStringDateStart] = useState('Tanggal Awal');
  const [dateEnd, setDateEnd] = useState(new Date());
  const [stringDateEnd, setStringDateEnd] = useState('Tanggal Akhir');
  const [isShowDate, toggleShowDate] = useState(false);
  const [isShowDate2, toggleShowDate2] = useState(false);
  const [isModalDateOpen, toggleModalDate] = useState(false);

  const [checkedValue, setCheckedValue] = useState([
    {id: 1, value: 'isContentNumber', isChecked: false, text: 'No. Surat'},
    {id: 2, value: 'isCreatedDate', isChecked: false, text: 'Tanggal Dibuat'},
    {id: 3, value: 'isSender', isChecked: true, text: 'Pengirim'},
    {id: 4, value: 'isSubject', isChecked: true, text: 'Perihal'},
    {id: 5, value: 'isRecipient', isChecked: true, text: 'Penerima'},
    {id: 6, value: 'isUnread', isChecked: false, text: 'Yang belum dibaca'},
    {
      id: 7,
      value: 'isNoAttachment',
      isChecked: false,
      text: 'Yang tidak ada lampiran',
    },
  ]);
  const [isSearchOpen, toggleOpenSearch] = useState(false);
  const [isLoading, setLoading] = useState(true);

  const {params = {}} = state;
  const paramDesc = useRef(params.paramDesc);
  const setParam = useRef(setParams);

  useEffect(() => {
    setParam.current({openModal: () => toggleOpenSearch(true)});
    setParam.current({openDate: () => toggleModalDate(true)});
  }, [paramDesc, setParam]);

  useEffect(() => {
    const getListSurat = async () => {
      const token = await AsyncStorage.getItem('mainLoginToken');

      console.log('keyword:', keyword);

      await apiContent
        .post('/getContentInList', {
          token,
          limit: 10,
          offset: 0,
          keyword,
          dateStart:
            stringDateStart == 'Tanggal Awal' ? '01-01-2019' : stringDateStart,
          dateEnd:
            stringDateEnd == 'Tanggal Akhir' ? '31-12-2019' : stringDateEnd,

          isContentNumber: checkedValue[0].isChecked,
          isCreatedDate: checkedValue[1].isChecked,
          isSender: checkedValue[2].isChecked,
          isSubject: checkedValue[3].isChecked,
          isRecipient: checkedValue[4].isChecked,
          isUnread: checkedValue[5].isChecked,
          isNoAttachment: checkedValue[6].isChecked,
        })
        .then(resp => {
          const {code, error_message} = resp.data.system;
          const {rows} = resp.data.data;
          console.log(rows);

          // let a = rows[0].contentdate.substr(0, 10);

          if (code === 228) {
            setFetchedData(rows);
          } else {
            alert(error_message);
          }

          setLoading(false);
        })
        .catch(error => {
          alert('Gagal mengambil data');
          console.log(error);
        });
    };

    getListSurat();
  }, [keyword, checkedValue, stringDateStart, stringDateEnd]);

  const handleCheck = id => {
    let objId = checkedValue.findIndex(obj => obj.id == id);

    let valueBaru = checkedValue[objId];
    valueBaru = {...valueBaru, isChecked: !valueBaru.isChecked};

    let objBaru = Object.assign([...checkedValue], {[objId]: valueBaru});

    setCheckedValue(objBaru);
  };

  const handleDate = (event, pickedDate) => {
    toggleShowDate(false);

    if (pickedDate) {
      let dd = String(pickedDate.getDate()).padStart(2, '0');
      let mm = String(pickedDate.getMonth() + 1).padStart(2, '0'); //January is 0!
      let yyyy = pickedDate.getFullYear();

      let fullDate = dd + '-' + mm + '-' + yyyy;

      setDateStart(pickedDate);
      setStringDateStart(fullDate);
    }
  };

  const handleDate2 = (event, pickedDate) => {
    toggleShowDate2(false);

    if (pickedDate) {
      let dd = String(pickedDate.getDate()).padStart(2, '0');
      let mm = String(pickedDate.getMonth() + 1).padStart(2, '0'); //January is 0!
      let yyyy = pickedDate.getFullYear();

      let fullDate = dd + '-' + mm + '-' + yyyy;

      setDateEnd(pickedDate);
      setStringDateEnd(fullDate);
    }
  };

  return isLoading ? (
    <LoadingScreen />
  ) : (
    <>
      <StatusBar backgroundColor="#fff" barStyle="dark-content" />
      <ListSuratItem data={fetchedData} />

      {/* Modal Date  */}
      <Modal
        animationType="fade"
        transparent
        visible={isModalDateOpen}
        onRequestClose={() => toggleModalDate(false)}>
        <TouchableOpacity
          onPress={() => toggleModalDate(false)}
          style={styles.containerFreespace}
        />

        <View style={styles.containerModalContent}>
          <View style={styles.containerJudulModal}>
            <Text style={styles.textJudulSection}>Filter Tanggal</Text>
            <TouchableOpacity onPress={() => toggleModalDate(false)}>
              <IconAwesome name="times-circle" style={styles.iconClose} />
            </TouchableOpacity>
          </View>

          {isShowDate && (
            <DatePicker mode="date" value={dateStart} onChange={handleDate} />
          )}

          {isShowDate2 && (
            <DatePicker2 mode="date" value={dateEnd} onChange={handleDate2} />
          )}

          <View style={styles.containerOptionFilter}>
            <View style={{flexDirection: 'row'}}>
              <View>
                <Text>Tanggal Awal</Text>
                <TouchableOpacity
                  style={styles.containerTglModal}
                  onPress={() => toggleShowDate(true)}>
                  <Icon name="date" style={{fontSize: 25}} />
                  <Text
                    style={{
                      fontSize: 14,
                      fontFamily: 'OpenSans-SemiBold',
                      color: '#424242',
                      marginLeft: 15,
                    }}>
                    {stringDateStart}
                  </Text>
                </TouchableOpacity>
              </View>

              <View style={{marginHorizontal: 10}} />

              <View>
                <Text>Tanggal Akhir</Text>
                <TouchableOpacity
                  style={styles.containerTglModal}
                  onPress={() => toggleShowDate2(true)}>
                  <Icon name="date" style={{fontSize: 25}} />
                  <Text
                    style={{
                      fontSize: 14,
                      fontFamily: 'OpenSans-SemiBold',
                      color: '#424242',
                      marginLeft: 15,
                    }}>
                    {stringDateEnd}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={{marginVertical: 20}} />
          </View>

          <TouchableOpacity onPress={() => toggleModalDate(false)}>
            <LinearGradient
              style={styles.containerDuaTombolPalingBawah}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              colors={['#38ee7d', '#11988d']}>
              <Text style={styles.textTerapkan}>Terapkan</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </Modal>

      {/* Modal Search  */}
      <Modal
        animationType="fade"
        transparent
        visible={isSearchOpen}
        onRequestClose={() => toggleOpenSearch(false)}>
        <TouchableOpacity
          onPress={() => toggleOpenSearch(false)}
          style={styles.containerFreespace}
        />

        <View style={styles.containerModalContent}>
          <View style={styles.containerJudulModal}>
            <Text style={styles.textJudulSection}>Pencarian Surat</Text>
            <TouchableOpacity onPress={() => toggleOpenSearch(false)}>
              <IconAwesome name="times-circle" style={styles.iconClose} />
            </TouchableOpacity>
          </View>

          <TextInput
            placeholder="Ketik pencarian di sini..."
            placeholderTextColor="#424242"
            returnKeyType="done"
            value={keyword}
            onChangeText={setKeyword}
          />

          <View style={styles.containerOptionFilter}>
            <Text style={styles.textJudulSection}>Filter Surat</Text>

            <Text style={{fontSize: 16}}>Cari keyword berdasarkan: </Text>

            {checkedValue.map((item, key) => {
              return (
                <View key={key}>
                  <CheckItem
                    value={item.isChecked}
                    text={item.text}
                    onChange={() => handleCheck(item.id)}
                  />
                </View>
              );
            })}
          </View>

          {/* <TouchableOpacity
          onPress={handleResetCheckbox}
          style={[
            styles.containerDuaTombolPalingBawah,
            {borderColor: '#38ee7d', borderWidth: 1},
          ]}>
          <Text style={styles.textReset}>Reset</Text>
        </TouchableOpacity> */}

          <TouchableOpacity onPress={() => toggleOpenSearch(false)}>
            <LinearGradient
              style={styles.containerDuaTombolPalingBawah}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              colors={['#38ee7d', '#11988d']}>
              <Text style={styles.textTerapkan}>Terapkan</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </Modal>
    </>
  );
};

ListSurat.navigationOptions = ({navigation: {getParam, goBack}}) => ({
  headerTitle: getParam('title'),
  headerLeft: (
    <NavHeaderButtons>
      <Item
        title="back"
        color="#424242"
        iconName="arrow-left"
        onPress={() => goBack()}
      />
    </NavHeaderButtons>
  ),
  headerRight: (
    <NavHeaderButtons>
      <Item
        title="filter"
        color="#424242"
        iconName="filter"
        // iconName="date"
        // onPress={getParam('openDate')}
      />
      <Item
        title="search"
        color="#424242"
        iconName="search"
        onPress={getParam('openModal')}
      />
    </NavHeaderButtons>
  ),
});

const styles = StyleSheet.create({
  containerFreespace: {
    flex: 1,
    backgroundColor: '#000000',
    opacity: 0.6,
  },
  containerModalContent: {
    width: Dimensions.get('window').width,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 19,
    paddingVertical: 15,
  },
  containerJudulModal: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  containerOptionFilter: {
    marginTop: 15,
  },
  containerDuaTombolPalingBawah: {
    marginTop: 15,
    paddingTop: 7,
    paddingBottom: 10,
    borderRadius: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconClose: {
    fontSize: 25,
  },
  inputPencarian: {
    borderWidth: 1,
    borderColor: '#eeeeee',
    borderRadius: 3,
    paddingHorizontal: 15,
    paddingVertical: 8,
    fontSize: 14,
    fontFamily: 'OpenSans',
    marginTop: 10,
  },
  textJudulSection: {
    fontSize: 16,
    color: '#424242',
    fontFamily: 'OpenSans-Semibold',
  },
  textReset: {
    fontSize: 18,
    color: '#424242',
    fontFamily: 'SourceSansPro-Regular',
  },
  textTerapkan: {
    fontSize: 18,
    color: '#FFFFFF',
    fontFamily: 'SourceSansPro-Semibold',
  },
  modalSingleChoose: {
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height * 0.9,
    top: Dimensions.get('window').width * 0.1,
    borderRadius: 5,
    position: 'absolute',
    backgroundColor: 'white',
    elevation: 5,
    padding: 12,
  },
  containerModalTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    paddingHorizontal: 15,
  },
  containerTglModal: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 10,
    borderWidth: 1,
    borderColor: '#eeeeee',
    borderRadius: 3,
    paddingHorizontal: 17,
    paddingVertical: 9,
  },
});

export default ListSurat;
