import React, {useState, useEffect} from 'react';
import {Text, View, Button} from 'react-native';

import DocumentPicker from 'react-native-document-picker';
import RNFS from 'react-native-fs';
import RNFetchBlob from 'rn-fetch-blob';
import {apiAttachment} from 'rootapp/src/services/api';
import AsyncStorage from '@react-native-community/async-storage';

const CobaUploadFile = () => {
  const [fileData, setfileData] = useState([]);

  const ambilFile = async () => {
    try {
      const results = await DocumentPicker.pickMultiple({
        type: [DocumentPicker.types.allFiles],
      });

      let pickedData = [];

      for (const res of results) {
        const {uri, name, size} = res;

        await RNFS.readFile(uri, 'base64')
          .then(resBase64 => {
            pickedData = [...pickedData, {name, content: resBase64, size}];
          })
          .catch(err => console.log(err));
      }

      console.log(pickedData);
      setfileData(pickedData);
    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        throw err;
      }
    }
  };

  return (
    <View style={{padding: 30, paddingTop: 100}}>
      <Button title="Ambil File" onPress={ambilFile} />
      {/* <Button title="Ambil File" onPress={() => console.log(fileData)} /> */}

      {fileData.map((item, key) => {
        return <Text key={key}>{item.name}</Text>;
      })}
    </View>
  );
};

export default CobaUploadFile;
