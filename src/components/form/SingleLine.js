import React from 'react';
import {StyleSheet, TextInput} from 'react-native';

const FormSingleLine = ({placeholder, value, onChange}) => {
  return (
    <TextInput
      placeholder={placeholder}
      placeholderTextColor="#424242"
      value={value}
      onChangeText={onChange}
      style={styles.input}
    />
  );
};

const styles = StyleSheet.create({
  input: {
    flex: 1,
    marginTop: 10,
    borderWidth: 1,
    borderColor: '#eeeeee',
    borderRadius: 3,
    fontSize: 15,
    fontFamily: 'OpenSans-SemiBold',
    color: '#424242',
    paddingHorizontal: 17,
    paddingVertical: 9,
  },
});

export default FormSingleLine;
