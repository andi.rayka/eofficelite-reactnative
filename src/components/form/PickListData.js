import React from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';

import Icon from 'react-native-vector-icons/Fontisto';

const PickListData = ({onPress, text}) => {
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <Icon name="nav-icon-a" size={20} />
      <Text style={styles.text}>{text}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 10,
    borderWidth: 1,
    borderColor: '#eeeeee',
    borderRadius: 3,
    paddingHorizontal: 17,
    paddingVertical: 13,
  },
  text: {
    fontSize: 15,
    fontFamily: 'OpenSans-SemiBold',
    color: '#424242',
    marginLeft: 17,
  },
});

export default PickListData;
