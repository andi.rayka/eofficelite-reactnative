import React from 'react';
import {TouchableOpacity, Text} from 'react-native';

import DatePicker from '@react-native-community/datetimepicker';
import Icon from 'react-native-vector-icons/Fontisto';

const FormTanggal = ({isShowDate, value, onChange, stringDate, onPress}) => {
  return (
    <>
      {isShowDate && (
        <DatePicker mode="date" value={value} onChange={onChange} />
      )}

      <TouchableOpacity
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          marginTop: 10,
          borderWidth: 1,
          borderColor: '#eeeeee',
          borderRadius: 3,
          paddingHorizontal: 17,
          paddingVertical: 9,
        }}
        onPress={onPress}>
        <Icon name="date" style={{fontSize: 25}} />
        <Text
          style={{
            fontSize: 14,
            fontFamily: 'OpenSans-SemiBold',
            color: '#424242',
            marginLeft: 15,
          }}>
          {stringDate}
        </Text>
      </TouchableOpacity>
    </>
  );
};

export default FormTanggal;
