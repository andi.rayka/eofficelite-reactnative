import React from 'react';
import {Text, TouchableOpacity, FlatList, StyleSheet} from 'react-native';

import {connect} from 'react-redux';
import {
  pilihIntContact,
  pilihExtContact,
  pilihUnit,
  pilihTopik,
} from '../../redux/buatSuratMasuk';
import {withNavigation} from 'react-navigation';

const ListFormDataItem = ({
  data,
  type,
  navigation: {goBack},
  pilihIntContact,
  pilihExtContact,
  pilihUnit,
  pilihTopik,
}) => {
  const onChoose = (data1, data2) => {
    switch (type) {
      case 'topic':
        pilihTopik(data1, data2);
        goBack();
        break;

      case 'externalContact':
        pilihExtContact(data1, data2);
        goBack();
        break;

      // case 'internalContact':
      //   pilihIntContact(data1, data2);
      //   goBack();
      //   break;

      case 'unit':
        pilihUnit(data1, data2);
        goBack();
        break;

      default:
        alert('error menyimpan data');
        break;
    }
  };

  const onChooseSementara = (a, b, c, d, e) => {
    switch (type) {
      case 'internalContact':
        pilihIntContact(a, b, c, d, e);
        goBack();
        break;
    }
  };

  const renderItem = ({item}) => {
    switch (type) {
      case 'topic':
        const {idcontenttopic, topictext} = item;
        return (
          <TouchableOpacity
            activeOpacity={0.6}
            style={{padding: 15}}
            onPress={() => onChoose(idcontenttopic, topictext)}>
            <Text style={{fontSize: 16}}>{topictext}</Text>
          </TouchableOpacity>
        );

      case 'externalContact':
        const {idext, exttext} = item;
        return (
          <TouchableOpacity
            activeOpacity={0.6}
            style={{padding: 15}}
            onPress={() => onChoose(idext, exttext)}>
            <Text style={{fontSize: 16}}>{exttext}</Text>
          </TouchableOpacity>
        );

      case 'internalContact':
        const {positionname, nameunit} = item;
        return (
          <TouchableOpacity
            activeOpacity={0.6}
            style={{padding: 15}}
            onPress={() =>
              onChooseSementara(
                item.iduser,
                item.idposition,
                item.idunit,
                item.rec_int_text,
                positionname,
              )
            }>
            <Text style={{fontSize: 16}}>{positionname}</Text>
            <Text style={{fontSize: 16}}>{nameunit}</Text>
          </TouchableOpacity>
        );

      case 'unit':
        const {idunit, unit_name} = item;
        return (
          <TouchableOpacity
            activeOpacity={0.6}
            style={{padding: 15}}
            onPress={() => onChoose(idunit, unit_name)}>
            <Text style={{fontSize: 16}}>{unit_name}</Text>
          </TouchableOpacity>
        );

      default:
        return (
          <Text style={{fontSize: 16}}>
            Gagal mengambil data, keluar aplikasi dan ulangi lagi
          </Text>
        );
    }
  };

  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      data={data}
      renderItem={renderItem}
      keyExtractor={(item, key) => key.toString()}
    />
  );
};

const styles = StyleSheet.create({});

const mapDispatchToProps = dispatch => {
  return {
    pilihIntContact: (a, b, c, d, e) =>
      dispatch(pilihIntContact(a, b, c, d, e)),
    pilihExtContact: (id, text) => dispatch(pilihExtContact(id, text)),
    pilihUnit: (id, text) => dispatch(pilihUnit(id, text)),
    pilihTopik: (id, text) => dispatch(pilihTopik(id, text)),
  };
};

const Component = withNavigation(ListFormDataItem);

export default connect(
  null,
  mapDispatchToProps,
)(Component);
