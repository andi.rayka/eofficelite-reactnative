import React, {useState, useEffect} from 'react';
import {Text, TouchableOpacity, FlatList, View} from 'react-native';

import {withNavigation} from 'react-navigation';

const ListSelectDataMultiChoice = ({data, type}) => {
  const [copyData, setCopyData] = useState([]);

  useEffect(() => {
    let arr = data;
    for (let i = 0; i < data.length; i++) {
      arr[i] = {...arr[i], isChecked: false};
    }

    console.log('penerima disposisi', arr);

    setCopyData(arr);
  }, [data]);

  const handleSelect = id => {
    let objId = copyData.findIndex(obj => obj.iduser == id);

    let valueBaru = copyData[objId];
    valueBaru = {...valueBaru, isChecked: !valueBaru.isChecked};
    console.log('valuebaru', valueBaru);

    let objBaru = Object.assign([...copyData], {[objId]: valueBaru});

    setCopyData(objBaru);

    // Pass checked item ke parent
    let kasihParent = [];

    for (let i = 0; i < objBaru.length; i++) {
      const item = objBaru[i];
      const objBuatParent = {
        iduser: item.iduser,
        idposition: item.idposition,
        idunit: item.idunit,
        textint_recipient: item.rec_int_text,
      };

      if (item.isChecked) {
        kasihParent = [...kasihParent, objBuatParent];
      }
    }

    setCopyData(kasihParent);
    console.log('nyampe kasihparent', kasihParent);
  };

  const renderItem = ({item}) => {
    switch (type) {
      case 'dispositionReceiverInt':
        const {positionname, nameunit} = item;
        const {iduser} = item;

        return (
          <TouchableOpacity
            activeOpacity={0.6}
            style={{
              marginHorizontal: 15,
              marginVertical: 5,
              paddingHorizontal: 10,
              paddingVertical: 5,
              backgroundColor: item.isChecked ? '#b2b0b0' : '#fff',
              borderRadius: 3,
            }}
            onPress={() => handleSelect(iduser)}>
            <Text>{positionname}</Text>
            <Text style={{fontSize: 16}}>{nameunit}</Text>
          </TouchableOpacity>
        );

      default:
        return (
          <Text style={{fontSize: 16}}>
            Gagal mengambil data, keluar aplikasi dan ulangi lagi
          </Text>
        );
    }
  };

  return (
    <>
      <FlatList
        showsVerticalScrollIndicator={false}
        data={data}
        renderItem={renderItem}
        keyExtractor={(item, key) => key.toString()}
      />
      <View style={{marginTop: 20}} />
    </>
  );
};

export default ListSelectDataMultiChoice;
