import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  SectionList,
  StyleSheet,
  Image,
} from 'react-native';

const ListSuratItem = ({data}) => {
  const renderSectionHeader = ({section: {title}}) => {
    return <Text style={styles.textSection}>{title}</Text>;
  };

  const renderItem = ({item}) => {
    return (
      <TouchableOpacity activeOpacity={0.6} style={styles.containerList}>
        <View
          style={{
            borderRadius: 50,
            backgroundColor: 'purple',
            width: 45,
            height: 45,
          }}
        />
        <View style={styles.containerContent}>
          <Text style={styles.textDate}>{item.time}</Text>
          <Text style={styles.textContent}>
            <Text style={styles.textDate}>{item.sender}</Text>
            {' ' + item.content}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <SectionList
      showsVerticalScrollIndicator={false}
      sections={data}
      renderItem={renderItem}
      renderSectionHeader={renderSectionHeader}
      keyExtractor={(item, key) => key.toString()}
    />
  );
};

const styles = StyleSheet.create({
  containerList: {
    flexDirection: 'row',
    paddingHorizontal: 15,
    backgroundColor: '#FFFFFF',
    marginTop: 17,
  },
  containerContent: {
    marginLeft: 10,
    flex: 1,
  },
  textSection: {
    fontSize: 17,
    fontFamily: 'OpenSans-SemiBold',
    color: '#424242',
    paddingHorizontal: 15,
    marginTop: 15,
  },
  textDate: {
    fontSize: 15,
    fontFamily: 'SourceSansPro-SemiBold',
    color: '#212121',
  },
  textContent: {
    fontSize: 15,
    fontFamily: 'SourceSansPro-Regular',
    color: '#616161',
  },
});

export default ListSuratItem;
