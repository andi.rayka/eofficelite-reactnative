import React from 'react';
import {View, StyleSheet, TextInput} from 'react-native';

import Icon from 'react-native-vector-icons/Fontisto';

const SearchFormItem = ({value, onChange}) => {
  const {containerUtama, iconSearch, inputText} = styles;

  return (
    <View style={containerUtama}>
      <Icon name="search" style={iconSearch} />
      <TextInput
        autoFocus
        style={inputText}
        underlineColorAndroid="transparent"
        returnKeyType="go"
        value={value}
        onChangeText={onChange}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  containerUtama: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 15,
    borderBottomColor: 'grey',
    borderBottomWidth: 0.5,
  },
  inputText: {
    flex: 1,
    marginLeft: 10,
    fontSize: 17,
  },
  iconSearch: {
    fontSize: 22,
  },
});

export default SearchFormItem;
