import React, {useState} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  Modal,
  Dimensions,
  StyleSheet,
  TextInput,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient/';
// import CheckBox from '@react-native-community/checkbox';

const CheckItem = ({value, text, onChange}) => {
  return (
    <TouchableOpacity
      onPress={onChange}
      style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
      <CheckBox value={value} onValueChange={onChange} />
      <Text>{text}</Text>
    </TouchableOpacity>
  );
};

const ModalSearch = ({isModalOpen, closeModal, onSet}) => {
  const [keyword, setKeyword] = useState('');
  const [checkedValue, setCheckedValue] = useState([
    {id: 1, value: 'isContentNumber', text: 'Nomor surat', isChecked: 1},
    {id: 2, value: 'isCreatedDate', text: 'Tanggal dibuat', isChecked: 0},
    {id: 3, value: 'isSender', text: 'Pengirim', isChecked: 1},
    {id: 4, value: 'isSubject', text: 'Judul surat', isChecked: 1},
    {id: 5, value: 'isRecipient', text: 'Penerima', isChecked: 1},
    {id: 6, value: 'isUnread', text: 'Yang belum dibaca', isChecked: 0},
    {
      id: 7,
      value: 'isNoAttachment',
      text: 'Yang tidak ada lampiran',
      isChecked: 0,
    },
  ]);

  const handleCheck = id => {
    // let newArray = [...checkedValue]
    // newArray[id-1] =
    // let obj = {id: id-1, }
    // setCheckedValue(a=> {return {...a, a[id-1].isChecked}} )

    console.log('check');
  };

  const resetCheckedBox = () => {
    // for (let i = 0; i < checkedValue.length; i++) {
    //   const item = checkedValue[i];
    //   // setCheckedValue((isChecked = 0));
    //   console.log(item);
    // }

    setCheckedValue(checkedValue.map(item => (item.isChecked = 0)));
    console.log(checkedValue);
  };

  return (
    <Modal
      animationType="fade"
      transparent
      visible={isModalOpen}
      onRequestClose={closeModal}>
      <TouchableOpacity
        onPress={closeModal}
        style={styles.containerFreespace}
      />

      <View style={styles.containerModalContent}>
        <View style={styles.containerJudulModal}>
          <Text style={styles.textJudulSection}>Pencarian Surat</Text>
          <TouchableOpacity onPress={closeModal}>
            <Icon name="times-circle" style={styles.iconClose} />
          </TouchableOpacity>
        </View>

        <TextInput
          placeholder="Ketik pencarian di sini..."
          placeholderTextColor="#424242"
          returnKeyType="done"
          value={keyword}
          onChangeText={setKeyword}
        />

        <View style={styles.containerOptionFilter}>
          <Text style={styles.textJudulSection}>Filter Surat</Text>

          <Text style={{fontSize: 16}}>Cari berdasarkan: </Text>

          {checkedValue.map((item, key) => {
            return (
              <CheckItem
                key={item.id}
                value={Boolean(item.isChecked)}
                text={item.text}
                onChange={() => handleCheck(item.id)}
              />
            );
          })}
        </View>

        <TouchableOpacity
          onPress={resetCheckedBox}
          style={[
            styles.containerDuaTombolPalingBawah,
            {borderColor: '#38ee7d', borderWidth: 1},
          ]}>
          <Text style={styles.textReset}>Reset</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            onSet(keyword);
            closeModal();
          }}>
          <LinearGradient
            style={styles.containerDuaTombolPalingBawah}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={['#38ee7d', '#11988d']}>
            <Text style={styles.textTerapkan}>Terapkan</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  containerFreespace: {
    flex: 1,
    backgroundColor: '#000000',
    opacity: 0.6,
  },
  containerModalContent: {
    width: Dimensions.get('window').width,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 19,
    paddingVertical: 15,
  },
  containerJudulModal: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  containerOptionFilter: {
    marginTop: 15,
  },
  containerDuaTombolPalingBawah: {
    marginTop: 15,
    paddingTop: 7,
    paddingBottom: 10,
    borderRadius: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconClose: {
    fontSize: 25,
  },
  inputPencarian: {
    borderWidth: 1,
    borderColor: '#eeeeee',
    borderRadius: 3,
    paddingHorizontal: 15,
    paddingVertical: 8,
    fontSize: 14,
    fontFamily: 'OpenSans',
    marginTop: 10,
  },
  textJudulSection: {
    fontSize: 16,
    color: '#424242',
    fontFamily: 'OpenSans-Semibold',
  },
  textReset: {
    fontSize: 18,
    color: '#424242',
    fontFamily: 'SourceSansPro-Regular',
  },
  textTerapkan: {
    fontSize: 18,
    color: '#FFFFFF',
    fontFamily: 'SourceSansPro-Semibold',
  },
});

export default ModalSearch;
