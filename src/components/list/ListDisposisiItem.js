import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  StyleSheet,
  Image,
} from 'react-native';

import Icon from 'react-native-vector-icons/Feather';

import {withNavigation} from 'react-navigation';

const ListDisposisiItem = ({data, navigation: {navigate}}) => {
  const renderItem = ({item}) => {
    return (
      <TouchableOpacity
        activeOpacity={0.6}
        style={styles.containerList}
        onPress={() => navigate('DetailDisposisiMasuk')}>
        <View flex={1}>
          <Text>haha</Text>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      data={data}
      renderItem={renderItem}
      keyExtractor={(item, key) => key.toString()}
    />
  );
};

const styles = StyleSheet.create({
  containerList: {
    flexDirection: 'row',
    paddingTop: 19,
    paddingBottom: 24,
    paddingHorizontal: 15,
    backgroundColor: '#FFFFFF',
  },
  containerTextAtas: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  containerBarisBawah: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 8,
    justifyContent: 'space-between',
  },
  containerLampiran: {
    flexDirection: 'row',
    justifyContent: 'center',
    borderWidth: 0.5,
    borderColor: '#38ee7d',
    padding: 3,
  },
  containerStatus: {
    // backgroundColor: '#37d67a',
    backgroundColor: '#ff8a65',
    borderRadius: 20,
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  iconDisposisi: {
    width: 40,
    height: 30,
  },
  textPengirim: {
    flex: 1,
    fontSize: 16,
    color: '#424242',
    fontFamily: 'SourceSansPro-SemiBold',
  },
  textTraitCode: {
    fontSize: 13,
    color: '#fff',
    fontFamily: 'SourceSansPro-SemiBold',
    textAlign: 'center',
    backgroundColor: '#2ccce4',
    width: '100%',
    marginTop: 2,
    padding: 1,
    borderRadius: 1.5,
  },
  textTgl: {
    fontSize: 14,
    color: '#424242',
    fontFamily: 'SourceSansPro-SemiBold',
    marginLeft: 10,
  },
  textIsi: {
    fontSize: 14,
    color: '#424242',
    fontFamily: 'SourceSansPro-SemiBold',
  },
  textLampiran: {
    fontSize: 16,
    color: '#424242',
    fontFamily: 'SourceSansPro-Regular',
    marginLeft: 5,
  },
  textStatus: {
    fontSize: 13,
    color: '#FFFFFF',
    fontFamily: 'SourceSansPro-SemiBold',
  },
});

export default withNavigation(ListDisposisiItem);
