import React, {useState} from 'react';
import {
  View,
  TouchableOpacity,
  Text,
  Modal,
  StyleSheet,
  Dimensions,
  TextInput,
} from 'react-native';

import {connect} from 'react-redux';
import {setSettings} from '../../redux/pilihListSurat';
import Icon from 'react-native-vector-icons/FontAwesome';
import LinearGradient from 'react-native-linear-gradient/';
// import CheckBox from '@react-native-community/checkbox';

const CheckItem = ({value, text, onChange}) => {
  return (
    <TouchableOpacity
      onPress={onChange}
      style={{flexDirection: 'row', alignItems: 'center', marginTop: 5}}>
      <CheckBox value={value} onValueChange={onChange} />
      <Text>{text}</Text>
    </TouchableOpacity>
  );
};

const ModalSearch = ({
  isModalOpen,
  closeModal,
  getList,
  isContentNumber,
  isCreatedDate,
  isSender,
  isSubject,
  isRecipient,
  isUnread,
  isNoAttachment,
  setSettings,
}) => {
  const [keyword, setKeyword] = useState('');
  const [isContentNumber_, toggleisContentNumber] = useState(
    Boolean(isContentNumber),
  );
  const [isCreatedDate_, toggleisCreatedDate] = useState(
    Boolean(isCreatedDate),
  );
  const [isSender_, toggleisSender] = useState(Boolean(isSender));
  const [isSubject_, toggleisSubject] = useState(Boolean(isSubject));
  const [isRecipient_, toggleisRecipient] = useState(Boolean(isRecipient));
  const [isUnread_, toggleisUnread] = useState(Boolean(isUnread));
  const [isNoAttachment_, toggleisNoAttachment] = useState(
    Boolean(isNoAttachment),
  );

  return (
    <Modal
      animationType="fade"
      transparent
      visible={isModalOpen}
      onRequestClose={closeModal}>
      <TouchableOpacity
        onPress={closeModal}
        style={styles.containerFreespace}
      />

      <View style={styles.containerModalContent}>
        <View style={styles.containerJudulModal}>
          <Text style={styles.textJudulSection}>Pencarian Surat</Text>
          <TouchableOpacity onPress={closeModal}>
            <Icon name="times-circle" style={styles.iconClose} />
          </TouchableOpacity>
        </View>

        <TextInput
          placeholder="Ketik pencarian di sini..."
          placeholderTextColor="#424242"
          returnKeyType="done"
          value={keyword}
          onChangeText={setKeyword}
        />

        <View style={styles.containerOptionFilter}>
          <Text style={styles.textJudulSection}>Filter Surat</Text>

          <View>
            <Text style={{fontSize: 16}}>Cari berdasarkan: </Text>

            <CheckItem
              text="Nomor surat"
              value={isContentNumber_}
              onChange={() => toggleisContentNumber(!isContentNumber_)}
            />
            <CheckItem
              text="Tanggal dibuat"
              value={isCreatedDate_}
              onChange={() => toggleisCreatedDate(!isCreatedDate_)}
            />
            <CheckItem
              text="Pengirim"
              value={isSender_}
              onChange={() => toggleisSender(!isSender_)}
            />
            <CheckItem
              text="Judul surat"
              value={isSubject_}
              onChange={() => toggleisSubject(!isSubject_)}
            />
            <CheckItem
              text="Penerima"
              value={isRecipient_}
              onChange={() => toggleisRecipient(!isRecipient_)}
            />
            <CheckItem
              text="Yang belum dibaca"
              value={isUnread_}
              onChange={() => toggleisUnread(!isUnread_)}
            />
            <CheckItem
              text="Yang tidak ada lampiran"
              value={isNoAttachment_}
              onChange={() => toggleisNoAttachment(!isNoAttachment_)}
            />
          </View>
        </View>

        <TouchableOpacity
          onPress={() => {
            toggleisContentNumber(false);
            toggleisCreatedDate(false);
            toggleisSender(false);
            toggleisSubject(false);
            toggleisRecipient(false);
            toggleisUnread(false);
            toggleisNoAttachment(false);
          }}
          style={[
            styles.containerDuaTombolPalingBawah,
            {borderColor: '#38ee7d', borderWidth: 1},
          ]}>
          <Text style={styles.textReset}>Reset</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            setSettings(
              keyword,
              +isContentNumber_,
              +isCreatedDate,
              +isSender_,
              +isSubject_,
              +isRecipient_,
              +isUnread_,
              +isNoAttachment_,
            );
            closeModal();
            // setTimeout(() => {
            //   getList();
            // }, 3000);
            getList();
          }}>
          <LinearGradient
            style={styles.containerDuaTombolPalingBawah}
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={['#38ee7d', '#11988d']}>
            <Text style={styles.textTerapkan}>Terapkan</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  containerFreespace: {
    flex: 1,
    backgroundColor: '#000000',
    opacity: 0.6,
  },
  containerModalContent: {
    width: Dimensions.get('window').width,
    backgroundColor: '#FFFFFF',
    paddingHorizontal: 19,
    paddingVertical: 15,
  },
  containerJudulModal: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  containerOptionFilter: {
    marginTop: 15,
  },
  containerDuaTombolPalingBawah: {
    marginTop: 15,
    paddingTop: 7,
    paddingBottom: 10,
    borderRadius: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconClose: {
    fontSize: 25,
  },
  inputPencarian: {
    borderWidth: 1,
    borderColor: '#eeeeee',
    borderRadius: 3,
    paddingHorizontal: 15,
    paddingVertical: 8,
    fontSize: 14,
    fontFamily: 'OpenSans',
    marginTop: 10,
  },
  textJudulSection: {
    fontSize: 16,
    color: '#424242',
    fontFamily: 'OpenSans-Semibold',
  },
  textReset: {
    fontSize: 18,
    color: '#424242',
    fontFamily: 'SourceSansPro-Regular',
  },
  textTerapkan: {
    fontSize: 18,
    color: '#FFFFFF',
    fontFamily: 'SourceSansPro-Semibold',
  },
});

const mapDispatchToProps = dispatch => {
  return {
    setSettings: (
      keyword,
      isContentNumber,
      isCreatedDate,
      isSender,
      isSubject,
      isRecipient,
      isUnread,
      isNoAttachment,
    ) =>
      dispatch(
        setSettings(
          keyword,
          isContentNumber,
          isCreatedDate,
          isSender,
          isSubject,
          isRecipient,
          isUnread,
          isNoAttachment,
        ),
      ),
  };
};

export default connect(
  null,
  mapDispatchToProps,
)(ModalSearch);
